# BudgetBuddy

![BudgetBuddy logotype](logotype.png)
[![GitLab Licens](https://img.shields.io/gitlab/license/BrunoArmondBraga%2Fbudget-buddy)](LICENSE)

BudgetBuddy is an Android application designed to help users manage their finances efficiently. It allows users to organize their expenses into different categories based on their preferences and easily share expenses with others, making bill splitting effortless.


## Installing and running the app
First, download the necessary files from the [latest release](https://gitlab.com/BrunoArmondBraga/budget-buddy/-/releases) or [build it from source](#building-from-source). 

The app requires a running server connection. To start the server, run the command:

```console
java -jar file
```

Where `file` is the compiled server file.

To run the application, simply install the `.apk` on an Android environment (device or emulator) and open the newly installed app. To connect to the server, the app will request the device IP address where the server is running. Currently, the connection only works if both devices are on the same network, for example, both connected to the same Wi-Fi.

To find the network IP address of the device where the server is running, execute the following command:

```console
ifconfig | grep "inet " | grep -v 127.0.0.1
```

The IP address should be in the format `192.168.x.x`.


## Building from source
First, clone the repository using the command:

```console
git clone https://gitlab.com/BrunoArmondBraga/budget-buddy/
```

To build the server, run the following commands:

```console
cd budget-buddy/ktor-project
./gradlew build
```

The compiled file will be available at `budget-buddy/ktor-project/build/libs/com.example.ktor-budgetbuddy-all.jar`.

To build the app installer (`.apk`), run the following command:

```console
cd budget-buddy/android-project
./gradlew assembleDebug
```

Note: You need to download the Android SDK and set it's path in an `ANDROID_HOME` environment variable.

The installer will be available at `budget-buddy/android-project/app/build/outputs/apk/debug/app-debug.apk`.

## Technologies
[![Kotlin](https://img.shields.io/badge/Kotlin-0095D5?&style=for-the-badge&logo=kotlin&logoColor=white)](https://kotlinlang.org)
[![Android Studio](https://img.shields.io/badge/android%20studio-346ac1?style=for-the-badge&logo=android%20studio&logoColor=white)](https://developer.android.com/studio)
[![Jetpack Compose](https://img.shields.io/badge/Jetpack%20Compose-4285F4.svg?style=for-the-badge&logo=Jetpack-Compose&logoColor=white)](https://developer.android.com/develop/ui/compose)
[![Ktor](https://img.shields.io/badge/Ktor-087CFA.svg?style=for-the-badge&logo=Ktor&logoColor=white)](https://ktor.io) 
[![Postman](https://img.shields.io/badge/Postman-FF6C37.svg?style=for-the-badge&logo=Postman&logoColor=white)](https://www.postman.com/)
[![Exposed](https://img.shields.io/badge/Exposed-orange?style=for-the-badge)](https://github.com/JetBrains/Exposed)
[![H2](https://img.shields.io/badge/H2-blue?style=for-the-badge)](https://github.com/h2database/h2database)

## Developers
- Bruno Armond Braga                  
- Guilherme Vinicius Ferreira de Assis