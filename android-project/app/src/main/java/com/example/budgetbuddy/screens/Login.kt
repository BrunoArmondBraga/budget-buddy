package com.example.budgetbuddy.screens

import android.content.res.Configuration
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.State
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.budgetbuddy.components.Logotype
import com.example.budgetbuddy.components.TextField
import com.example.budgetbuddy.components.UnderlinedTextButton
import com.example.budgetbuddy.components.UpdateEffect
import com.example.budgetbuddy.ui.theme.BudgetBuddyTheme
import com.example.budgetbuddy.viewmodel.AppViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

@Composable
fun LoginScreen(
    checkLogin: (username: String, password: String) -> Unit,
    successfulLogin: (username: String, password: String) -> Unit,
    onCreateAccountClick: () -> Unit,
    successState: State<AppViewModel.Status>
) {
    val snackbarHostState = remember { SnackbarHostState() }
    var username: String by remember { mutableStateOf("") }
    var password: String by remember { mutableStateOf("") }

    Scaffold(
        snackbarHost = {
            SnackbarHost(hostState = snackbarHostState)
        }
    ) {innerPadding ->
        Column(
            Modifier
                .padding(innerPadding)
                .fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Logotype()
            Spacer(Modifier.size(20.dp))
            Text(
                "Login",
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.bodyLarge
            )

            TextField(
                value = username,
                onChange = { username = it },
                label = "Username",
                modifier = Modifier.padding(8.dp)
            )

            TextField(
                value = password,
                onChange = { password = it },
                label = "Password",
                visualTransformation = PasswordVisualTransformation()
            )

            Button(
                modifier = Modifier.padding(8.dp),
                onClick = { checkLogin(username, password) },
            ) {
                Text("Login", style = MaterialTheme.typography.bodyLarge)
            }

            UnderlinedTextButton("or create account", onCreateAccountClick)
        }
    }

    UpdateEffect(successState.value) {
        if (successState.value.status) {
            successfulLogin(username, password)
        } else {
            snackbarHostState.showSnackbar("Login failed. Please try again.")
        }
    }
}

@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    name = "DefaultPreviewDark"
)
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    name = "DefaultPreviewLight"
)
@Composable
fun LoginScreenPreview() {
    BudgetBuddyTheme {
        LoginScreen(
            { _,_ -> },
            { _,_ -> },
            {},
            MutableStateFlow(AppViewModel.Status(false)).collectAsState()
        )
    }
}
