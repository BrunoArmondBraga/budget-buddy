package com.example.budgetbuddy.screens

import android.content.res.Configuration
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberTopAppBarState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.budgetbuddy.components.BarTitle
import com.example.budgetbuddy.components.FilledButton
import com.example.budgetbuddy.components.TextField
import com.example.budgetbuddy.components.TopBarTemplate
import com.example.budgetbuddy.ui.theme.BudgetBuddyTheme

@Composable
fun ConnectSection(
    onConnectClick: (code: String) -> Unit,
    modifier: Modifier = Modifier
) {
    Column (
        modifier
            .padding(32.dp)
            .fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(14.dp),
    ) {
        Text(
            text = "Enter budget code:",
            style = MaterialTheme.typography.titleLarge
        )

        var code: String by remember { mutableStateOf("") }
        TextField(
            value = code,
            onChange = { code = it },
            label = "Budget unique code"
        )

        FilledButton(
            content = { Text(text = "Connect", style = MaterialTheme.typography.bodyLarge) },
            onClick = { onConnectClick(code) }
        )
    }

}

@Preview(showBackground = true)
@Composable
fun ConnectSectionPreview() {
    ConnectSection({})
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ConnectToBudgetScreen(
    onConnectClick: (code: String) -> Unit,
) {
    val scrollBehavior = TopAppBarDefaults.pinnedScrollBehavior(rememberTopAppBarState())

    Scaffold(
        Modifier
            .fillMaxSize()
            .nestedScroll(scrollBehavior.nestedScrollConnection),

        topBar = { TopBarTemplate(title = { BarTitle("Connect") }) },
    ) { innerPadding ->
        ConnectSection(onConnectClick, Modifier.padding(innerPadding))
    }
}


@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    name = "DefaultPreviewDark"
)
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    name = "DefaultPreviewLight"
)
@Composable
fun ConnectToBudgetScreenPreview() {
    BudgetBuddyTheme {
        ConnectToBudgetScreen {}
    }
}