package com.example.budgetbuddy.screens

import android.os.Parcelable
import com.example.core.User
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.RawValue
import kotlinx.serialization.Serializable

@Serializable
sealed class Routes() {

    @Serializable
    data object Home : Routes()

    @Serializable
    data object SetIP : Routes()

    @Serializable
    data object Login : Routes()

    @Serializable
    data object CreateAccount : Routes()

    @Serializable
    data object EditProfile : Routes()

    @Serializable
    data object UserBudgets : Routes()

    @Serializable
    data object ConnectToBudget : Routes()

    @Serializable
    data object CreateBudget : Routes()

    @Serializable
    data object Budget : Routes()

    @Serializable
    data object EditBudget : Routes()

    @Serializable
    data object Category : Routes()

    @Serializable
    data object CreateCategory : Routes()

    @Serializable
    data object EditCategory : Routes()

    @Serializable
    data object Expense : Routes()

    @Serializable
    data object CreateExpense : Routes()

    @Serializable
    data object EditExpense : Routes()

    @Serializable
    data object AddPayment : Routes()
}