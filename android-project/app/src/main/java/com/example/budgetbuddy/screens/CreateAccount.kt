package com.example.budgetbuddy.screens

import android.content.res.Configuration
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.budgetbuddy.components.Logotype
import com.example.budgetbuddy.components.TextField
import com.example.budgetbuddy.ui.theme.BudgetBuddyTheme


@Composable
fun CreateAccountSection(
    onCreateAccountClick: (name: String, username: String, password: String) -> Unit
) {
    Column (
        Modifier
            .padding(24.dp)
            .fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(8.dp)
    ) {
        Text(
            "Create account",
            textAlign = TextAlign.Center,
            style = MaterialTheme.typography.bodyLarge
        )

        var name: String by remember { mutableStateOf("") }
        TextField(
            value = name,
            onChange = { name = it },
            label = "Name",
            modifier = Modifier.fillMaxWidth()
        )

        var username: String by remember { mutableStateOf("") }
        TextField(
            value = username,
            onChange = { username = it },
            label = "Username",
            modifier = Modifier.fillMaxWidth()
        )

        var password: String by remember { mutableStateOf("") }
        TextField(
            value = password,
            onChange = { password = it },
            label = "Password",
            modifier = Modifier.fillMaxWidth(),
            visualTransformation = PasswordVisualTransformation()
        )

        Button(
            onClick = { onCreateAccountClick(name, username, password) },
        ) {
            Text("Create", style = MaterialTheme.typography.bodyLarge)
        }

    }
}

@Preview(showBackground = true)
@Composable
fun CreateAccountSectionPreview() {
    CreateAccountSection(onCreateAccountClick = { _, _, _ -> })
}

@Composable
fun CreateAccountScreen(
    onCreateAccountClick: (name: String, username: String, password: String) -> Unit
) {
    Surface(Modifier.fillMaxSize()) {
        Column(
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Logotype()
            Spacer(Modifier.size(20.dp))
            Row(Modifier.fillMaxWidth(.8f)) {
                CreateAccountSection(onCreateAccountClick)
            }
        }
    }
}

@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    name = "DefaultPreviewDark"
)
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    name = "DefaultPreviewLight"
)
@Composable
fun CreateAccountScreenPreview() {
    BudgetBuddyTheme {
        CreateAccountScreen { _, _, _ -> }
    }
}