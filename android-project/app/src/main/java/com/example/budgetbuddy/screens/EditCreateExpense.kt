package com.example.budgetbuddy.screens

import android.annotation.SuppressLint
import android.content.res.Configuration
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.Checkbox
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberTopAppBarState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.budgetbuddy.components.CategoryTopBar
import com.example.budgetbuddy.components.FABIconSize
import com.example.budgetbuddy.components.FloatingButton
import com.example.budgetbuddy.components.ReceiptButton
import com.example.budgetbuddy.components.TextField
import com.example.budgetbuddy.components.UserPreview
import com.example.budgetbuddy.ui.theme.BudgetBuddyTheme
import com.example.core.Budget
import com.example.core.Category
import com.example.core.Expense
import com.example.core.User

@Composable
fun MultiSelectDropdown(selectedUsers: MutableState<Set<User>>, budgetUsers: List<User>) {
    var expanded by remember { mutableStateOf(false) }

    Column(modifier = Modifier.padding(16.dp)) {
        OutlinedButton(
            onClick = { expanded = true },
            shape = RoundedCornerShape(6.dp),
            modifier = Modifier.fillMaxWidth()
        ) {
            Text("Select users", style = MaterialTheme.typography.bodyLarge)
        }

        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false }
        ) {
            budgetUsers.forEach { user ->
                val isSelected = user in selectedUsers.value
                DropdownMenuItem(
                    text = {
                        Row(
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Checkbox(checked = isSelected, onCheckedChange = null)
                            Text(text = user.name, modifier = Modifier.padding(start = 8.dp))
                        }
                    },
                    onClick = {
                        selectedUsers.value = if (isSelected) {
                            selectedUsers.value - user
                        } else {
                            selectedUsers.value + user
                        }
                    }
                )
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun EditExpenseScreen(
    creationMode: Boolean = false,
    expense: Expense,
    currentBudget: Budget,
    currentCategory: Category,
    budgetUsers: List<User> = emptyList(),
    onConfirm: (oldExpense: Expense, newExpense: Expense) -> Unit,
    onDelete: (expense: Expense) -> Unit,
    onReceiptClick: (receipt: String) -> Unit,
) {
    val scrollBehavior = TopAppBarDefaults.pinnedScrollBehavior(rememberTopAppBarState())

    var expenseName: String by remember { mutableStateOf(expense.name) }
    var expenseDescription: String by remember { mutableStateOf(expense.description) }
    var expenseTotalValue: String by remember { mutableStateOf(expense.totalValue.toString()) }
    var expenseDate: String by remember { mutableStateOf(expense.expenseDate) }
    val selectedUsers = remember { mutableStateOf(setOf<User>()) }


    Scaffold(
        Modifier
            .fillMaxSize()
            .nestedScroll(scrollBehavior.nestedScrollConnection),

        topBar = {
            CategoryTopBar(currentBudget, currentCategory, true)
        },
        floatingActionButton = {
            FloatingButton(
                onClick = { onConfirm(expense,
                    Expense(
                        expenseName,
                        expenseDescription,
                        expense.expensePayer,
                        expenseDate,
                        expenseTotalValue.toFloat(),
                        0f,
                        selectedUsers.value.toList(),
                        ""
                    )
                ) },
                content = {
                    Icon(
                        Icons.Filled.Check,
                        "Create category",
                        Modifier.size(FABIconSize)
                    )
                })
        }
    ) { innerPadding ->
        Column (
            verticalArrangement = Arrangement.spacedBy(16.dp),
            modifier = Modifier
                .padding(innerPadding)
                .padding(horizontal = 32.dp)
                .fillMaxWidth()
        ) {
            Text(
                "Expense",
                modifier = Modifier.fillMaxWidth(),
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.displaySmall
            )
            TextField(
                value = expenseName,
                onChange = { expenseName = it },
                label = "Name",
                Modifier.fillMaxWidth()
            )
            TextField(
                value = expenseDescription,
                onChange = { expenseDescription = it },
                label = "Description",
                Modifier.fillMaxWidth()
            )
            if (creationMode) {
                TextField(
                    value = expenseTotalValue.toString(),

                    onChange = { expenseTotalValue = it },
                    label = "Value",
                    Modifier.fillMaxWidth()
                )
            } else {
                Text("Value: ${expense.totalValue}", style = MaterialTheme.typography.headlineSmall)
            }

            Row (
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.spacedBy(8.dp)
            ) {
                Text("Payed by", style = MaterialTheme.typography.titleLarge)
                UserPreview(user = expense.expensePayer)
                Text("on", style = MaterialTheme.typography.titleLarge)
                TextField(
                    value = expenseDate,
                    onChange = { expenseDate = it },
                    label = "Date",
                    modifier = Modifier.padding(4.dp)
                )
            }

            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.spacedBy(8.dp)
            ) {
                Text(text = "Debtors: ", style = MaterialTheme.typography.titleLarge)

                if (creationMode) {
                    MultiSelectDropdown(selectedUsers, budgetUsers)
                }
            }

            if (!creationMode) {
                LazyColumn(
                    verticalArrangement = Arrangement.spacedBy(12.dp)
                ) {
                    items(expense.debtors) { debtor -> UserPreview(debtor) }
                }
            }

            ReceiptButton(
                onClick = { onReceiptClick(expense.receipt) },
                textStyle = MaterialTheme.typography.titleLarge, )


            Spacer(Modifier.size(64.dp))
            if (!creationMode) {
                IconButton(onClick = { onDelete(expense) },
                    Modifier.align(Alignment.CenterHorizontally)) {
                    Icon(
                        Icons.Filled.Delete,
                        "Delete category",
                        modifier = Modifier.size(32.dp)
                    )
                }
            }
        }
    }
}

@Composable
fun CreateExpenseScreen(
    currentUser: User,
    currentBudget: Budget,
    currentCategory: Category,
    budgetUsers: List<User>,
    onCreate: (newExpense: Expense) -> Unit,
    onReceiptClick: (receipt: String) -> Unit,
) {
    EditExpenseScreen(
        true,
        expense = Expense(
            "",
            "",
            currentUser,
            "",
            0f,
            0f,
            emptyList(),
            ""
        ),
        currentBudget = currentBudget,
        currentCategory = currentCategory,
        budgetUsers = budgetUsers,
        onConfirm = { _: Expense, newExpense: Expense
            -> onCreate(newExpense) },
        onDelete = {},
        onReceiptClick = onReceiptClick
    )
}

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@OptIn(ExperimentalMaterial3Api::class)
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    name = "DefaultPreviewDark"
)
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    name = "DefaultPreviewLight"
)
@Composable
fun CreateExpenseScreenPreview() {
    BudgetBuddyTheme {
        CreateExpenseScreen(
            User("", "User"),
            Budget("", "My Budget"),
            Category("Category", "Star"),
            listOf(
              User("", "UserA"),
                User("", "UserB"),
                User("", "UserC")
            ),
            { _, -> },
            { _ -> }
        )
    }
}

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@OptIn(ExperimentalMaterial3Api::class)
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    name = "DefaultPreviewDark"
)
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    name = "DefaultPreviewLight"
)
@Composable
fun EditExpenseScreenPreview() {
    BudgetBuddyTheme {
        EditExpenseScreen(
            expense = Expense(
                "Expense name",
                "Expense description",
                User("", "User Name"),
                "xx/xx/xx",
                0f,
                0f,
                listOf(User("", "User"), User("", "OtherUser")),
                ""
            ),
            currentBudget = Budget("", "My Budget"),
            currentCategory = Category("Category", "Star"),
            onConfirm = { _, _ -> },
            onDelete = { _ -> },
            onReceiptClick = {}
        )
    }
}