package com.example.budgetbuddy

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.budgetbuddy.screens.BudgetScreen
import com.example.budgetbuddy.screens.CategoryScreen
import com.example.budgetbuddy.screens.ConnectToBudgetScreen
import com.example.budgetbuddy.screens.CreateAccountScreen
import com.example.budgetbuddy.screens.CreateBudgetScreen
import com.example.budgetbuddy.screens.CreateCategoryScreen
import com.example.budgetbuddy.screens.CreateExpenseScreen
import com.example.budgetbuddy.screens.EditBudgetScreen
import com.example.budgetbuddy.screens.EditCategoryScreen
import com.example.budgetbuddy.screens.EditExpenseScreen
import com.example.budgetbuddy.screens.EditProfileScreen
import com.example.budgetbuddy.screens.ExpenseScreen
import com.example.budgetbuddy.screens.HomeScreen
import com.example.budgetbuddy.screens.LoginScreen
import com.example.budgetbuddy.screens.Routes
import com.example.budgetbuddy.screens.SetIPScreen
import com.example.budgetbuddy.screens.UserBudgetsScreen
import com.example.budgetbuddy.ui.theme.BudgetBuddyTheme
import com.example.budgetbuddy.viewmodel.AppViewModel
import com.example.core.Budget
import com.example.core.Category
import com.example.core.Expense
import com.example.core.PartialPayment
import com.example.core.User
import com.network.KtorClient

class MainActivity : ComponentActivity() {

    private var ktorClient = KtorClient()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            BudgetBuddyTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val navController = rememberNavController()
                    val viewModel: AppViewModel by viewModels { AppViewModel.factory(ktorClient) }

                    NavHost(navController = navController, startDestination = Routes.SetIP) {
                        composable<Routes.SetIP> {
                            SetIPScreen(
                                checkConnection = { ip ->
                                    ktorClient.setIP(ip)
                                    viewModel.testConnection()
                                },
                                successfulConnection = { ip ->
                                    navController.navigate(Routes.Home) {
                                        popUpTo(Routes.SetIP) { inclusive = true }
                                        launchSingleTop = true
                                    }
                                },
                                viewModel.connectionSuccess.collectAsState()
                            )
                        }

                        composable<Routes.Home> {
                            HomeScreen(
                                onTap = {
                                    if (viewModel.currentUser != null) {
                                        navController.navigate(Routes.UserBudgets)
                                    } else {
                                        navController.navigate(Routes.Login)
                                    }
                                }
                            )
                        }

                        composable<Routes.Login> {
                            LoginScreen(
                                checkLogin = { username, password ->
                                    viewModel.login(username, password) },
                                successfulLogin = { _, _ ->
                                    navController.navigate(Routes.UserBudgets) {
                                        popUpTo(Routes.Login) { inclusive = true }
                                    }
                                },
                                onCreateAccountClick = { navController.navigate(Routes.CreateAccount) },
                                viewModel.loginSuccess.collectAsState()
                            )
                        }

                        composable<Routes.CreateAccount> {
                            CreateAccountScreen(
                                onCreateAccountClick = { name, username, password ->
                                    viewModel.createUser(username, name, password)
                                    navController.popBackStack()
                                },
                            )
                        }

                        composable<Routes.UserBudgets> {
                            LaunchedEffect(true) { viewModel.loadUserBudgets() }

                            if (viewModel.userBudgets != null) {
                                UserBudgetsScreen(
                                    onProfileClick = { navController.navigate(Routes.EditProfile) },
                                    onBudgetClick = { budget: Budget ->
                                        viewModel.setBudget(budget)
                                        navController.navigate(Routes.Budget)
                                    },
                                    onConnectClick = { navController.navigate(Routes.ConnectToBudget) },
                                    onCreateClick = { navController.navigate(Routes.CreateBudget) },
                                    currentUser = viewModel.currentUser!!,
                                    userBudgets = viewModel.userBudgets!!
                                )
                            } else {
                                // TODO: Show loading screen
                            }

                        }

                        composable<Routes.EditProfile> {
                            viewModel.currentUser?.let { user ->
                                EditProfileScreen(
                                    user = user,
                                    onLogoutClick = {
                                        viewModel.logout()
                                        navController.navigate(Routes.Home) {
                                            popUpTo(Routes.UserBudgets) { inclusive = true }
                                            launchSingleTop = true
                                        }
                                    },
                                    onConfirm = { newName: String ->
                                        viewModel.editProfile(newName)
                                        navController.popBackStack()
                                    }
                                )
                            }
                        }

                        composable<Routes.ConnectToBudget> {
                            ConnectToBudgetScreen(
                                onConnectClick = { code: String ->
                                    viewModel.connectToBudget(code)
                                    viewModel.setBudget(code)
                                    if (viewModel.currentBudget != null) {
                                        navController.navigate(Routes.Budget) {
                                            popUpTo(Routes.ConnectToBudget) { inclusive = true }
                                        }
                                    } else {
                                        // TODO: Show error message
                                    }
                                }
                            )
                        }

                        composable<Routes.CreateBudget> {
                            LaunchedEffect(true) { viewModel.getUnusedBudgetId() }

                            viewModel.unusedId?.let { id ->
                                CreateBudgetScreen(
                                    id = id,
                                    currentUser = viewModel.currentUser!!,
                                    onConfirm = { newBudget: Budget ->
                                        viewModel.createBudget(newBudget)
                                        viewModel.setBudget(newBudget)
                                        navController.navigate(Routes.Budget) {
                                            popUpTo(Routes.CreateBudget) { inclusive = true }
                                        }
                                    }
                                )
                            }
                        }

                        composable<Routes.Budget> {
                            LaunchedEffect(true) { viewModel.loadBudgetCategories() }

                            viewModel.budgetCategories?.let { budgetCategories ->
                                BudgetScreen(
                                    onCreateClick = { navController.navigate(Routes.CreateCategory) },
                                    onSettingsClick = { navController.navigate(Routes.EditBudget) },
                                    onCategoryClick = { category: Category ->
                                        viewModel.setCategory(category)
                                        navController.navigate(Routes.Category)
                                    },
                                    budget = viewModel.currentBudget!!,
                                    categories = budgetCategories
                                )
                            }
                        }

                        composable<Routes.EditBudget> {
                            LaunchedEffect(true) { viewModel.loadBudgetUsers() }

                            viewModel.budgetUsers?.let { budgetUsers ->
                                EditBudgetScreen(
                                    budget = viewModel.currentBudget!!,
                                    currentUser = viewModel.currentUser!!,
                                    budgetUsers = budgetUsers,
                                    onConfirm = { newBudget ->
                                        viewModel.editBudget(newBudget.name)
                                        navController.popBackStack()
                                    },
                                    onRemoveUser = { user ->
                                        viewModel.removeBudgetUser(user)
                                        viewModel.loadBudgetUsers()
                                    },
                                    onExitBudget = {
                                        viewModel.removeBudgetUser(viewModel.currentUser!!)
                                        navController.popBackStack()
                                        navController.popBackStack()
                                    }
                                )
                            }
                        }


                        composable<Routes.CreateCategory> {
                            CreateCategoryScreen(
                                viewModel.currentBudget!!,
                                onCreate = { newCategory: Category ->
                                    viewModel.createCategory(newCategory)
                                    navController.popBackStack()
                                }
                            )
                        }

                        composable<Routes.Category> {
                            LaunchedEffect(true) { viewModel.loadCategoryExpenses() }

                            viewModel.categoryExpenses?.let { categoryExpenses ->
                                CategoryScreen(
                                    onCreate = { navController.navigate(Routes.CreateExpense) },
                                    onEditCategoryClick = { navController.navigate(Routes.EditCategory) },
                                    onExpenseClick = { expense: Expense ->
                                        viewModel.setExpense(expense)
                                        navController.navigate(Routes.Expense)
                                    },
                                    checkPayment = { user: User, expense: Expense ->
                                        user == expense.expensePayer || !expense.debtors.contains(
                                            user
                                        )
                                    },
                                    currentUser = viewModel.currentUser!!,
                                    currentBudget = viewModel.currentBudget!!,
                                    category = viewModel.currentCategory!!,
                                    expenses = categoryExpenses
                                )
                            }
                        }

                        composable<Routes.EditCategory> {
                            EditCategoryScreen(
                                currentBudget = viewModel.currentBudget!!,
                                category = viewModel.currentCategory!!,
                                onConfirm = { oldCategory: Category, newCategory: Category ->
                                    viewModel.editCategory(
                                        oldCategory,
                                        newCategory.name,
                                        newCategory.icon
                                    )
                                    navController.popBackStack()
                                },
                                onDelete = { category: Category ->
                                    viewModel.deleteCategory(category)
                                    navController.popBackStack()
                                    navController.popBackStack()
                                }
                            )
                        }

                        composable<Routes.CreateExpense> {
                            LaunchedEffect(true) { viewModel.loadBudgetUsers() }

                            viewModel.budgetUsers?.let { budgetUsers ->
                                CreateExpenseScreen(
                                    currentUser = viewModel.currentUser!!,
                                    currentBudget = viewModel.currentBudget!!,
                                    currentCategory = viewModel.currentCategory!!,
                                    budgetUsers = budgetUsers.filter { it != viewModel.currentUser!! },
                                    onCreate = { newExpense: Expense ->
                                        viewModel.createExpense(newExpense)
                                        navController.popBackStack()
                                    },
                                    onReceiptClick = {}
                                )
                            }
                        }

                        composable<Routes.Expense> {
                            val getDebt = { expense: Expense, _: User ->
                                expense.remainingValue / expense.debtors.size }

                            LaunchedEffect(true) { viewModel.loadPartialPayments() }

                            viewModel.expensePartialPayments?.let { partialPayments ->
                                ExpenseScreen(
                                    currentUser = viewModel.currentUser!!,
                                    currentBudget = viewModel.currentBudget!!,
                                    currentCategory = viewModel.currentCategory!!,
                                    expense = viewModel.currentExpense!!,
                                    partialPayments = partialPayments,
                                    onEditExpenseClick = { navController.navigate(Routes.EditExpense) },
                                    onReceiptClick = { },
                                    onAddPartialPaymentClick = { expense: Expense ->
                                        viewModel.addPartialPayment(
                                            PartialPayment(
                                                viewModel.currentUser!!,
                                                getDebt(expense, viewModel.currentUser!!),
                                                ""
                                            )
                                        )
                                        viewModel.loadPartialPayments()
                                    },
                                    getDebt = getDebt
                                )
                            }
                        }


                        composable<Routes.EditExpense> {
                            EditExpenseScreen(
                                expense = viewModel.currentExpense!!,
                                currentBudget = viewModel.currentBudget!!,
                                currentCategory = viewModel.currentCategory!!,
                                budgetUsers = viewModel.budgetUsers!!,
                                onConfirm = { oldExpense: Expense, newExpense: Expense ->
                                    viewModel.editExpense(
                                        oldExpense,
                                        newExpense.name,
                                        newExpense.description,
                                        newExpense.expenseDate,
                                        newExpense.receipt
                                    )
                                    navController.popBackStack()
                                },
                                onDelete = { expense: Expense ->
                                    viewModel.deleteExpense(expense)
                                    navController.popBackStack()
                                    navController.popBackStack()
                                },
                                onReceiptClick = {}
                            )
                        }
                    }
                }
            }
        }
    }
}


