package com.example.budgetbuddy.screens

import android.annotation.SuppressLint
import android.content.res.Configuration
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.Logout
import androidx.compose.material.icons.filled.Cancel
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Logout
import androidx.compose.material.icons.filled.RemoveCircle
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberTopAppBarState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.budgetbuddy.components.BarTitle
import com.example.budgetbuddy.components.BudgetTopBar
import com.example.budgetbuddy.components.CategoryTopBar
import com.example.budgetbuddy.components.FABIconSize
import com.example.budgetbuddy.components.FloatingButton
import com.example.budgetbuddy.components.TextField
import com.example.budgetbuddy.components.TopBarTemplate
import com.example.budgetbuddy.components.UserPreview
import com.example.budgetbuddy.ui.theme.BudgetBuddyTheme
import com.example.core.Budget
import com.example.core.Category
import com.example.core.User

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun EditBudgetScreen(
    creationMode: Boolean = false,
    budget: Budget,
    currentUser: User,
    budgetUsers: List<User>,
    onConfirm: (newBudget: Budget) -> Unit,
    onRemoveUser: (user: User) -> Unit,
    onExitBudget: () -> Unit
) {
    val scrollBehavior = TopAppBarDefaults.pinnedScrollBehavior(rememberTopAppBarState())

    var budgetName: String by remember { mutableStateOf(budget.name) }

    Scaffold(
        Modifier
            .fillMaxSize()
            .nestedScroll(scrollBehavior.nestedScrollConnection),

        topBar = { TopBarTemplate(title = { BarTitle("Budget") }) },
        floatingActionButton = {
            FloatingButton(
                onClick = { onConfirm(Budget(budget.id, budgetName)) },
                content = {
                    Icon(
                        Icons.Filled.Check,
                        "Create category",
                        Modifier.size(FABIconSize)
                    )
                })
        }
    ) { innerPadding ->
        Column (
            verticalArrangement = Arrangement.spacedBy(16.dp),
            modifier = Modifier
                .padding(innerPadding)
                .padding(horizontal = 32.dp)
                .fillMaxWidth()
        ) {
            //Text("Name", style = MaterialTheme.typography.headlineSmall)
            TextField(value = budgetName, onChange = { budgetName = it }, label = "Budget Name")
            Text("ID", style = MaterialTheme.typography.headlineSmall)
            Text(budget.id, style = MaterialTheme.typography.bodyLarge)
            Text("Users", style = MaterialTheme.typography.headlineSmall)
            budgetUsers.forEach {user ->
                Row (verticalAlignment = Alignment.CenterVertically) {
                    UserPreview(user = user)
                    if (user != currentUser) {
                        IconButton(onClick = { onRemoveUser(user) }) {
                            Icon(
                                Icons.Filled.Cancel,
                                contentDescription = "Remove user",
                                Modifier.size(16.dp)
                            )
                        }
                    }
                }
            }

            Spacer(Modifier.size(64.dp))
            if (!creationMode) {
                IconButton(
                    onClick = onExitBudget,
                    Modifier.align(Alignment.CenterHorizontally)) {
                    Icon(
                        Icons.AutoMirrored.Filled.Logout,
                        "Delete category",
                        modifier = Modifier.size(32.dp)
                    )
                }
            }
        }
    }
}

@Composable
fun CreateBudgetScreen(
    id: String,
    currentUser: User,
    onConfirm: (newBudget: Budget) -> Unit,
) {
    EditBudgetScreen(
        true,
        budget = Budget(id, ""),
        currentUser = currentUser,
        budgetUsers = listOf(currentUser),
        onConfirm = onConfirm,
        onRemoveUser = {},
        onExitBudget = {}
    )
}

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@OptIn(ExperimentalMaterial3Api::class)
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    name = "DefaultPreviewDark"
)
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    name = "DefaultPreviewLight"
)
@Composable
fun CreateBudgetScreenPreview() {
    BudgetBuddyTheme {
        CreateBudgetScreen("###", User("", "User"), {},)
    }
}