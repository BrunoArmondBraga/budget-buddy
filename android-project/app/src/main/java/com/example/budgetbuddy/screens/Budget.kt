package com.example.budgetbuddy.screens

import android.content.res.Configuration
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberTopAppBarState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.budgetbuddy.components.BudgetTopBar
import com.example.budgetbuddy.components.FABIconSize
import com.example.budgetbuddy.components.FilledButton
import com.example.budgetbuddy.components.FloatingButton
import com.example.budgetbuddy.components.getIconByName
import com.example.budgetbuddy.ui.theme.BudgetBuddyTheme
import com.example.core.Budget
import com.example.core.Category

@Composable
fun CategoryCard(
    onCategoryClick: (category: Category) -> Unit,
    category: Category
) {
    val buttonSize = 88.dp
    FilledButton(
        content = {
            Icon(
                getIconByName(category.icon) as ImageVector,
                category.name,
                Modifier.size(buttonSize / 3)
            )
        },
        onClick = { onCategoryClick(category) },
        modifier = Modifier
            .padding(buttonSize / 10)
            .size(buttonSize)
    )
    Text(text = category.name, style = MaterialTheme.typography.bodyLarge)
}

@Composable
fun CategoriesList(
    onCategoryClick: (category: Category) -> Unit,
    categories: List<Category>,
    modifier: Modifier = Modifier
) {
    LazyVerticalGrid(
        columns = GridCells.Fixed(3),
        modifier = modifier.padding(16.dp)
    ) {

        items(categories) { category ->
            Column (
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier.padding(4.dp)
            ){
                CategoryCard(onCategoryClick, category)
            }

        }
    }
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun BudgetScreen(
    onCreateClick: () -> Unit,
    onSettingsClick: () -> Unit,
    onCategoryClick: (category: Category) -> Unit,
    budget: Budget,
    categories: List<Category>
) {
    val scrollBehavior = TopAppBarDefaults.pinnedScrollBehavior(rememberTopAppBarState())

    Scaffold(
        Modifier
            .fillMaxSize()
            .nestedScroll(scrollBehavior.nestedScrollConnection),

        topBar = { BudgetTopBar(budget, onSettingsClick = onSettingsClick) },
        floatingActionButton = {
            FloatingButton(
                onClick = onCreateClick,
                content = {
                    Icon(
                        Icons.Filled.Add,
                        "Create category",
                        Modifier.size(FABIconSize)
                    )
                })
        }
    ) { innerPadding ->
        CategoriesList(onCategoryClick, categories, Modifier.padding(innerPadding))
    }
}

@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    name = "DefaultPreviewDark"
)
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    name = "DefaultPreviewLight"
)
@Composable
fun BudgetScreenPreview() {
    BudgetBuddyTheme {
        BudgetScreen(
            onCreateClick = {},
            onSettingsClick = {},
            onCategoryClick = {},
            budget = Budget("", "Budget name"),
            categories = List(20)
            { index -> Category("${index + 1}th Category", "Star") }
        )
    }
}