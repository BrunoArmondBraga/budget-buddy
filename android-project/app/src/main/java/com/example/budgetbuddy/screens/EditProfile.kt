package com.example.budgetbuddy.screens

import android.content.res.Configuration
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.Logout
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.filled.Logout
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberTopAppBarState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.budgetbuddy.components.BarTitle
import com.example.budgetbuddy.components.FABIconSize
import com.example.budgetbuddy.components.FloatingButton
import com.example.budgetbuddy.components.TextField
import com.example.budgetbuddy.components.TopBarTemplate
import com.example.budgetbuddy.ui.theme.BudgetBuddyTheme
import com.example.core.User

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun EditProfileScreen(
    user: User,
    onLogoutClick: () -> Unit,
    onConfirm: (newName: String) -> Unit,
) {
    val scrollBehavior = TopAppBarDefaults.pinnedScrollBehavior(rememberTopAppBarState())
    var newName: String by remember { mutableStateOf(user.name) }

    Scaffold(
        Modifier
            .fillMaxSize()
            .nestedScroll(scrollBehavior.nestedScrollConnection),

        topBar = { TopBarTemplate(title = { BarTitle("Edit profile") }) },
        floatingActionButton = {
            FloatingButton(
                onClick = { onConfirm(newName) },
                content = {
                    Icon(
                        Icons.Filled.Check,
                        "Confirm edit",
                        Modifier.size(FABIconSize)
                    )
                })
        }
    ) { innerPadding ->
        Column (
            Modifier
                .padding(innerPadding)
                .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy(32.dp),
        ) {
            Icon(
                Icons.Filled.AccountCircle,
                "Edit photo",
                Modifier.size(140.dp)
            )

            TextField(
                value = newName,
                onChange = { newName = it },
                label = "Name",
            )

            IconButton(onClick = onLogoutClick, Modifier.padding(36.dp)) {
                Icon(Icons.AutoMirrored.Filled.Logout, contentDescription = "Logout")
            }
        }
    }
}


@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    name = "DefaultPreviewDark"
)
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    name = "DefaultPreviewLight"
)
@Composable
fun EditProfileScreenPreview() {
    BudgetBuddyTheme {
        EditProfileScreen(User("", "My name"), {}, {})
    }
}