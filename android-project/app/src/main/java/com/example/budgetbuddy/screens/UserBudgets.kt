package com.example.budgetbuddy.screens

import android.content.res.Configuration
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.AddCircleOutline
import androidx.compose.material.icons.filled.AddLink
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberTopAppBarState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.budgetbuddy.components.FABIconSize
import com.example.budgetbuddy.components.UserBudgetsTopBar
import com.example.budgetbuddy.components.FilledButton
import com.example.budgetbuddy.components.FloatingButton
import com.example.budgetbuddy.ui.theme.BudgetBuddyTheme
import com.example.core.Budget
import com.example.core.User

@Composable
fun BudgetsList(
    onBudgetClick: (budget: Budget) -> Unit,
    budgets: List<Budget>,
    modifier: Modifier = Modifier
) {
    LazyColumn(
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(12.dp)
    ) {
        items(budgets) { budget ->
            FilledButton(
                { Text(budget.name, style = MaterialTheme.typography.titleLarge) },
                { onBudgetClick(budget) },
                Modifier
                    .fillMaxWidth()
                    .height(60.dp)
                    .padding(horizontal = 12.dp)
            )
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun UserBudgetsScreen(
    onBudgetClick: (budget: Budget) -> Unit,
    onConnectClick: () -> Unit,
    onCreateClick: () -> Unit,
    onProfileClick: () -> Unit,
    currentUser: User,
    userBudgets: List<Budget>
) {
    val scrollBehavior = TopAppBarDefaults.pinnedScrollBehavior(rememberTopAppBarState())

    Scaffold(
        Modifier
            .fillMaxSize()
            .nestedScroll(scrollBehavior.nestedScrollConnection),

        topBar = { UserBudgetsTopBar(currentUser, onProfileClick) },
        floatingActionButton = {
            Column (verticalArrangement = Arrangement.spacedBy(8.dp)) {
                FloatingButton(
                    onClick = onConnectClick,
                    content = {
                        Icon(
                            Icons.Filled.AddLink,
                            "Connect to budget",
                            Modifier.size(FABIconSize)
                        )
                    }
                )
                FloatingButton(
                    onClick = onCreateClick,
                    content = {
                        Icon(
                            Icons.Filled.Add,
                            "Create new budget",
                            Modifier.size(FABIconSize)
                        )
                    }
                )
            }
        }
    ) { innerPadding ->
        BudgetsList(onBudgetClick, userBudgets, Modifier.padding(innerPadding))
    }
}

@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    name = "DefaultPreviewDark"
)
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    name = "DefaultPreviewLight"
)
@Composable
fun UserBudgetsScreenPreview() {
    BudgetBuddyTheme {
        UserBudgetsScreen(
            {},
            {},
            {},
            {},
            User("", "Name"),
            List(18)
                { index -> Budget("", "${index + 1}th Budget") }
        )
    }
}