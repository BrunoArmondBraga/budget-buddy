package com.example.budgetbuddy.screens

import android.content.res.Configuration
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.State
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.budgetbuddy.components.FilledButton
import com.example.budgetbuddy.components.TextField
import com.example.budgetbuddy.components.UpdateEffect
import com.example.budgetbuddy.ui.theme.BudgetBuddyTheme
import com.example.budgetbuddy.viewmodel.AppViewModel
import kotlinx.coroutines.flow.MutableStateFlow

@Composable
fun SetIPScreen(
    checkConnection: (ip: String) -> Unit,
    successfulConnection: (ip: String) -> Unit,
    successState: State<AppViewModel.Status>
) {
    val snackbarHostState = remember { SnackbarHostState() }
    var ip: String by remember { mutableStateOf("") }

    Scaffold(
        snackbarHost = {
            SnackbarHost(hostState = snackbarHostState)
        }
    ) { innerPadding ->
        Column (
            Modifier
                .padding(innerPadding)
                .padding(32.dp)
                .fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            Text(
                text = "Enter server localhost IP \n(192.168.x.x):",
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.titleLarge
            )

            TextField(
                value = ip,
                onChange = { ip = it },
                label = "IP",
                modifier = Modifier.padding(24.dp)
            )

            FilledButton(
                content = { Text(text = "Connect", style = MaterialTheme.typography.bodyLarge) },
                onClick = { checkConnection(ip) }
            )
        }
    }

    UpdateEffect(successState.value) {
        if (successState.value.status) {
            successfulConnection(ip)
        } else {
            snackbarHostState.showSnackbar("Connection failed. Please try again.")
        }
    }
}


@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    name = "DefaultPreviewDark"
)
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    name = "DefaultPreviewLight"
)
@Composable
fun SetIPScreenPreview() {
    BudgetBuddyTheme {
        SetIPScreen({}, {}, MutableStateFlow(AppViewModel.Status(false)).collectAsState())
    }
}