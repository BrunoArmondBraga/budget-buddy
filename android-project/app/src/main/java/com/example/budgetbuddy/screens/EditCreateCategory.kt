package com.example.budgetbuddy.screens

import android.content.res.Configuration
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberTopAppBarState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.budgetbuddy.components.BudgetTopBar
import com.example.budgetbuddy.components.FABIconSize
import com.example.budgetbuddy.components.FloatingButton
import com.example.budgetbuddy.components.TextField
import com.example.budgetbuddy.components.getIconByName
import com.example.budgetbuddy.components.iconNames
import com.example.budgetbuddy.ui.theme.BudgetBuddyTheme
import com.example.core.Budget
import com.example.core.Category

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun EditCategoryScreen(
    creationMode: Boolean = false,
    currentBudget: Budget,
    category: Category,
    onConfirm: (oldCategory: Category, newCategory: Category) -> Unit,
    onDelete: (category: Category) -> Unit,
) {
    val scrollBehavior = TopAppBarDefaults.pinnedScrollBehavior(rememberTopAppBarState())

    var categoryName: String by remember { mutableStateOf(category.name) }
    var categoryIcon: String by remember { mutableStateOf(category.icon) }

    Scaffold(
        Modifier
            .fillMaxSize()
            .nestedScroll(scrollBehavior.nestedScrollConnection),

        topBar = { BudgetTopBar(currentBudget, true) },
        floatingActionButton = {
            FloatingButton(
                onClick = { onConfirm(category, Category(categoryName, categoryIcon)) },
                content = {
                    Icon(
                        Icons.Filled.Check,
                        "Create category",
                        Modifier.size(FABIconSize)
                    )
                })
        }
    ) { innerPadding ->
        Column (
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy(24.dp),
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxWidth()
        ) {
            Text(
                "Category",
                style = MaterialTheme.typography.displaySmall
            )

            Surface(
                color = MaterialTheme.colorScheme.surfaceContainerHighest,
            ) {
                Box (
                    modifier = Modifier.padding(6.dp)
                ) {
                    Icon(
                        getIconByName(name = categoryIcon) as ImageVector,
                        "",
                        modifier = Modifier
                            .size(128.dp)
                            .padding(6.dp)
                    )
                    var expanded by remember { mutableStateOf(false) }
                    IconButton(
                        onClick = { expanded = true },
                        modifier = Modifier
                            .size(24.dp)
                            .align(Alignment.BottomEnd)
                    ) {
                        Icon(
                            Icons.Filled.Edit,
                            "Edit category icon"
                        )
                    }
                    DropdownMenu(
                        expanded = expanded,
                        onDismissRequest = { expanded = false }
                    ) {
                        iconNames.forEach {
                            DropdownMenuItem(
                                text = { },
                                onClick = {
                                    categoryIcon = it;
                                    expanded = false
                                          },
                                leadingIcon = {
                                    Icon(
                                        getIconByName(name = it) as ImageVector,
                                        "")
                                }
                            )
                        }
                    }
                }
            }

            TextField(
                value = categoryName,
                onChange = { categoryName = it },
                label = "Category name"
            )

            Spacer(Modifier.size(64.dp))
            if (!creationMode) {
                IconButton(onClick = { onDelete(category) }) {
                    Icon(
                        Icons.Filled.Delete,
                        "Delete category",
                        modifier = Modifier.size(32.dp)
                    )
                }
            }
        }
    }
}

@Composable
fun CreateCategoryScreen(
    currentBudget: Budget,
    onCreate: (newCategory: Category) -> Unit
) {
    EditCategoryScreen(
        true,
        currentBudget = currentBudget,
        category = Category("", "Star"),
        onConfirm = { _: Category, newCategory: Category -> onCreate(newCategory) },
        onDelete = {}
    )
}

@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    name = "DefaultPreviewDark"
)
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    name = "DefaultPreviewLight"
)
@Composable
fun CreateCategoryScreenPreview() {
    BudgetBuddyTheme {
        CreateCategoryScreen(currentBudget = Budget("", "Budget name")) {}
    }
}