package com.example.budgetbuddy.screens

import android.annotation.SuppressLint
import android.content.res.Configuration
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberTopAppBarState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.budgetbuddy.components.BudgetTopBar
import com.example.budgetbuddy.components.CategoryTopBar
import com.example.budgetbuddy.components.FABIconSize
import com.example.budgetbuddy.components.FilledButton
import com.example.budgetbuddy.components.FloatingButton
import com.example.budgetbuddy.components.UserPreview
import com.example.budgetbuddy.ui.theme.BudgetBuddyTheme
import com.example.core.Budget
import com.example.core.Category
import com.example.core.Expense
import com.example.core.User

@Composable
fun ExpenseCard(
    expense: Expense,
    onClick: () -> Unit,
    payed: Boolean
) {
    FilledButton(
        content = {
            Row (
                Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween
            ){
                Column {
                    Text(
                        text = expense.name,
                        style = MaterialTheme.typography.titleLarge
                    )
                    Text(
                        text = expense.description,
                        style = MaterialTheme.typography.bodySmall
                    )
                    UserPreview(expense.expensePayer)
                }
                Text(
                    text = expense.totalValue.toString(),
                    style = MaterialTheme.typography.displayMedium
                )
            }

        },
        onClick = onClick,
        Modifier
            .fillMaxWidth()
            .height(90.dp)
            .padding(horizontal = 12.dp),
        customColor = if (payed) null else MaterialTheme.colorScheme.error
    )
}


@Composable
fun ExpenseList(
    onExpenseClick: (expense: Expense) -> Unit,
    checkPayment: (user: User, expense: Expense) -> Boolean,
    user: User,
    expenses: List<Expense>,
    modifier: Modifier = Modifier,
) {
    LazyColumn(
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(12.dp)
    ) {
        items(expenses) {
            expense ->
            ExpenseCard(
                expense,
                { onExpenseClick(expense) },
                checkPayment(user, expense)
            )
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CategoryScreen(
    onCreate: () -> Unit,
    onEditCategoryClick: (category: Category) -> Unit,
    onExpenseClick: (expense: Expense) -> Unit,
    checkPayment: (user: User, expense: Expense) -> Boolean,
    currentUser: User,
    currentBudget: Budget,
    category: Category,
    expenses: List<Expense>
) {
    val scrollBehavior = TopAppBarDefaults.pinnedScrollBehavior(rememberTopAppBarState())

    Scaffold(
        Modifier
            .fillMaxSize()
            .nestedScroll(scrollBehavior.nestedScrollConnection),

        topBar = {
            CategoryTopBar(
                currentBudget = currentBudget,
                category = category,
                onEditCategoryClick = onEditCategoryClick
            )
        },
        floatingActionButton = {
            FloatingButton(
                onClick = onCreate,
                content = {
                    Icon(
                        Icons.Filled.Add,
                        "Create expense",
                        Modifier.size(FABIconSize)
                    )
                })
        }
    ) { innerPadding ->
        ExpenseList(
            onExpenseClick,
            checkPayment,
            currentUser,
            expenses,
            modifier = Modifier.padding(innerPadding),

        )
    }
}

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    name = "DefaultPreviewDark"
)
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    name = "DefaultPreviewLight"
)
@Composable
fun CategoryScreenPreview() {
    BudgetBuddyTheme {
        CategoryScreen(
            onCreate = {},
            onEditCategoryClick = {},
            onExpenseClick = {},
            checkPayment = { _,_ -> kotlin.random.Random.nextBoolean() },
            currentUser = User("1", ""),
            currentBudget = Budget("", "Budget name"),
            category = Category("Category name", "Star"),
            expenses = List(6)
                { index ->
                    Expense(
                        name ="${index + 1}th Expense",
                        description = "This is a fake expense. Don't pay it!",
                        expensePayer = User("", "Payer"),
                        expenseDate = "66/66/6666",
                        totalValue = 20f,
                        remainingValue = 75f,
                        debtors = emptyList(),
                        receipt = "",
                    )
                }
        )
    }
}