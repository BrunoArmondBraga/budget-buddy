package com.example.budgetbuddy.components

import android.content.res.Configuration
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.sp
import com.example.budgetbuddy.ui.theme.BudgetBuddyTheme
import androidx.compose.ui.text.font.FontWeight
import com.example.budgetbuddy.ui.theme.AlegreyaSansFontFamily
import com.example.budgetbuddy.ui.theme.LogotypeGreen

@Composable
fun Logotype(modifier: Modifier = Modifier, fontSize: TextUnit = 56.sp) {
    Text(
        text = "BudgetBuddy",
        fontFamily = AlegreyaSansFontFamily,
        fontWeight = FontWeight.Black,
        fontSize = fontSize,
        color = LogotypeGreen,
        modifier = modifier
    )
}

@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    name = "DefaultPreviewDark"
)
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    name = "DefaultPreviewLight"
)
@Composable
fun LogotypePreview() {
    BudgetBuddyTheme {
        Logotype()
    }
}