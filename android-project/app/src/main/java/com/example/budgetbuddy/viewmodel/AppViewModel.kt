package com.example.budgetbuddy.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.CreationExtras
import com.example.core.Budget
import com.example.core.Category
import com.example.core.Expense
import com.example.core.PartialPayment
import com.example.core.User
import com.network.KtorClient
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlin.random.Random


class AppViewModel(private val ktorClient: KtorClient) : ViewModel() {
    data class Status(val status: Boolean, val timestamp: Long = System.currentTimeMillis())

    val connectionSuccess = MutableStateFlow(Status(false))
    val loginSuccess = MutableStateFlow(Status(false))

    var currentUser by mutableStateOf<User?>(null)
        private set

    var userBudgets by mutableStateOf<List<Budget>?>(null)
        private set

    var currentBudget by mutableStateOf<Budget?>(null)
        private set

    var unusedId by mutableStateOf<String?>(null)
        private set

    var budgetCategories by mutableStateOf<List<Category>?>(null)
        private set

    var budgetUsers by mutableStateOf<List<User>?>(null)
        private set

    var currentCategory by mutableStateOf<Category?>(null)
        private set

    var categoryExpenses by mutableStateOf<List<Expense>?>(null)
        private set

    var currentExpense by mutableStateOf<Expense?>(null)
        private set

    var expensePartialPayments by mutableStateOf<List<PartialPayment>?>(null)
        private set


    fun testConnection() = viewModelScope.launch {
        connectionSuccess.value = Status(ktorClient.testIP())
    }

    fun login(username: String, password: String) = viewModelScope.launch {
        currentUser = ktorClient.verifyLogin(username, password)
        loginSuccess.value = Status(currentUser != null)
    }

    fun logout() {
        currentUser = null
        userBudgets = null
    }

    fun createUser(username: String, name: String, password: String) = viewModelScope.launch {
        ktorClient.createUser(username, name, password)
    }

    fun editProfile(newName: String) = viewModelScope.launch {
        currentUser = ktorClient.editProfile(currentUser!!, newName)
    }

    fun loadUserBudgets() = viewModelScope.launch {
        userBudgets = ktorClient.getUserBudgets(currentUser!!)
    }

    fun connectToBudget(id: String) = viewModelScope.launch {
        ktorClient.getBudget(id).let { ktorClient.addBudgetUser(it, currentUser!!) }
    }

    fun setBudget(budget: Budget) = viewModelScope.launch {
        currentBudget = budget
    }

    fun setBudget(id: String) = viewModelScope.launch {
        currentBudget = ktorClient.getBudget(id)
    }

    fun getUnusedBudgetId() = viewModelScope.launch {
        unusedId = ktorClient.getUnusedBudgetId()
    }

    fun createBudget(budget: Budget) = viewModelScope.launch {
        ktorClient.createBudget(currentUser!!, budget)
        userBudgets = userBudgets?.plus(budget)
    }

    fun editBudget(newName: String) = viewModelScope.launch {
        currentBudget = ktorClient.editBudget(currentBudget!!, newName)
    }

    fun loadBudgetCategories() = viewModelScope.launch {
        budgetCategories = ktorClient.getBudgetCategories(currentBudget!!)
    }

    fun loadBudgetUsers() = viewModelScope.launch {
        budgetUsers = ktorClient.getBudgetUsers(currentBudget!!)
    }

    fun removeBudgetUser(user: User) = viewModelScope.launch {
        ktorClient.removeBudgetUser(currentBudget!!, user)
    }

    fun setCategory(category: Category) = viewModelScope.launch {
        currentCategory = category
    }

    fun createCategory(category: Category) = viewModelScope.launch {
        ktorClient.addCategory(currentBudget!!, category)
    }

    fun editCategory(oldCategory: Category, newName: String, newIcon: String) =
        viewModelScope.launch {
            currentCategory = ktorClient.editCategory(currentBudget!!, currentCategory!!, newName, newIcon)
        }

    fun loadCategoryExpenses() = viewModelScope.launch {
        categoryExpenses = ktorClient.getCategoryExpenses(currentBudget!!, currentCategory!!)
    }

    fun deleteCategory(category: Category) = viewModelScope.launch {
        ktorClient.removeCategory(currentBudget!!, category)
    }

    fun setExpense(expense: Expense) = viewModelScope.launch {
        currentExpense = expense
    }

    fun createExpense(newExpense: Expense) = viewModelScope.launch {
        ktorClient.addExpense(currentBudget!!, currentCategory!!, newExpense)
    }


    fun editExpense(
        oldExpense: Expense,
        newName: String,
        newDescription: String,
        newExpenseDate: String,
        newReceipt: String
    ) = viewModelScope.launch {
        currentExpense = ktorClient.editExpense(
            currentBudget!!,
            currentCategory!!,
            oldExpense,
            newName,
            newDescription,
            newExpenseDate,
            newReceipt
        )
    }

    fun deleteExpense(expense: Expense) = viewModelScope.launch {
        ktorClient.removeExpense(currentBudget!!, currentCategory!!, expense)
        categoryExpenses = categoryExpenses?.filter { it != expense }
    }

    fun loadPartialPayments() = viewModelScope.launch {
        expensePartialPayments = ktorClient.getPartialPayments(currentExpense!!)
    }

    fun addPartialPayment(partialPayment: PartialPayment) = viewModelScope.launch {
        currentExpense = ktorClient.addPartialPayment(currentExpense!!, partialPayment)
    }


    companion object {
        fun factory(ktorClient: KtorClient): ViewModelProvider.Factory =
            object : ViewModelProvider.Factory {
                @Suppress("UNCHECKED_CAST")
                override fun <T : ViewModel> create(
                    modelClass: Class<T>,
                    extras: CreationExtras
                ): T {
                    return AppViewModel(ktorClient) as T
                }
            }
    }

}