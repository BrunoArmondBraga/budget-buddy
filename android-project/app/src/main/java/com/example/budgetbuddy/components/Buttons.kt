package com.example.budgetbuddy.components

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AddCircle
import androidx.compose.material.icons.filled.Receipt
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonColors
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.LargeFloatingActionButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import com.example.budgetbuddy.ui.theme.backgroundDark


@Composable
fun ReceiptButton(
    onClick: () -> Unit,
    textStyle: TextStyle = MaterialTheme.typography.bodyLarge
) {
    TextButton(onClick = onClick, contentPadding = PaddingValues(0.dp)) {
        Text(text = "Receipt: ", style = textStyle)
        Icon(Icons.Filled.Receipt, "Receipt")
    }
}

@Composable
fun AddPaymentButton(onClick: () -> Unit) {
    TextButton(onClick = onClick, contentPadding = PaddingValues(0.dp)) {
        Text(text = "Add payment", style = MaterialTheme.typography.bodyLarge)
        Icon(Icons.Filled.AddCircle, "Add payment", Modifier.padding(2.dp))
    }
}


@Composable
fun FilledButton(
    content: @Composable () -> Unit,
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    customColor: Color? = null,
    shape: Shape = RoundedCornerShape(10.dp),
) {
    val buttonColors = customColor?.let {
        ButtonDefaults.buttonColors(containerColor = it)
    } ?: ButtonDefaults.buttonColors()

    Button(onClick = onClick, modifier = modifier, colors = buttonColors, shape = shape) {
        content()
    }
}

@Composable
fun UnderlinedTextButton(
    text: String,
    onClick: () -> Unit,
    style: TextStyle = MaterialTheme.typography.bodyLarge,
) {
    TextButton(onClick) {
        Text(text, textDecoration = TextDecoration.Underline, style = style)
    }
}

@Composable
fun FloatingButton(
    onClick: () -> Unit,
    content: @Composable () -> Unit,
    modifier: Modifier = Modifier,
    shape: Shape = CircleShape,
) {
    LargeFloatingActionButton(
        onClick = onClick,
        modifier = modifier,
        shape = shape,
        content = content
    )
}