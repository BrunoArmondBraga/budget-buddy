package com.example.budgetbuddy.components

import android.annotation.SuppressLint
import android.content.res.Configuration
import android.content.res.Resources.Theme
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material.icons.outlined.Edit
import androidx.compose.material.icons.twotone.Edit
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.TopAppBarScrollBehavior
import androidx.compose.material3.rememberTopAppBarState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.budgetbuddy.ui.theme.BudgetBuddyTheme
import com.example.budgetbuddy.ui.theme.backgroundDark
import com.example.core.Budget
import com.example.core.Category
import com.example.core.User
import org.intellij.lang.annotations.JdkConstants.HorizontalAlignment

@Composable
fun BarTitle(title: String) {
    Text(title, style = MaterialTheme.typography.displayMedium, textAlign = TextAlign.Center)
}


/***************************
 * Top Bar Template
 ***************************/
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TopBarTemplate(
    title: @Composable () -> Unit = {},
    navigationIcon: @Composable () -> Unit = {},
    actions: @Composable RowScope.() -> Unit = {},
    scrollBehavior: TopAppBarScrollBehavior = TopAppBarDefaults.enterAlwaysScrollBehavior(rememberTopAppBarState())
) {
    CenterAlignedTopAppBar(
        modifier = Modifier.padding(12.dp),
        title = title,
        navigationIcon = navigationIcon,
        actions = actions,
        scrollBehavior = scrollBehavior,
    )
}


/***************************
 * CategoryScreen Top Bar
 ***************************/
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CategoryTopBar(
    currentBudget: Budget,
    category: Category,
    hideEditButton: Boolean = false,
    onEditCategoryClick: (category: Category) -> Unit = {}
) {
    TopBarTemplate(
        title = {
            Column (horizontalAlignment = Alignment.CenterHorizontally) {
                BarTitle(currentBudget.name)
                Row (
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.spacedBy(8.dp),
                    modifier = Modifier.offset(y = (-6).dp)
                ){
                    Icon(
                        getIconByName(category.icon) as ImageVector,
                        category.name,
                        Modifier.size(18.dp)
                    )
                    Text(text = category.name, style = MaterialTheme.typography.titleSmall)
                    if (!hideEditButton) {
                        IconButton(
                            onClick = { onEditCategoryClick(category) },
                            modifier = Modifier.size(10.dp),
                        ) {
                            Icon(
                                Icons.Filled.Edit,
                                contentDescription = "Edit category",
                                tint = MaterialTheme.colorScheme.primary
                            )
                        }
                    }
                }
            }
        }
    )
}

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@OptIn(ExperimentalMaterial3Api::class)
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    name = "DefaultPreviewDark"
)
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    name = "DefaultPreviewLight"
)
@Composable
fun CategoryTopBarPreview() {
    BudgetBuddyTheme {
        Scaffold(
            Modifier.fillMaxSize(),
            topBar = {
                CategoryTopBar(
                    currentBudget = Budget("", "Budget name"),
                    category = Category("Category name", "Star")
                )
            },
        ) {}
    }
}


/***************************
 * UserBudgetsScreen Top Bar
 ***************************/
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun UserBudgetsTopBar(
    currentUser: User,
    onProfileClick: () -> Unit
) {
    TopBarTemplate(
        title = {
            Column(
                Modifier.offset(y = (-4).dp),
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                Text(
                    currentUser.name,
                    Modifier.offset(y = 12.dp),
                    style = MaterialTheme.typography.titleSmall,
                )
                BarTitle("Budgets")
            }
        },
        actions = {
            IconButton(onClick = onProfileClick) {
                Icon(
                    Icons.Filled.AccountCircle,
                    "Edit profile",
                    Modifier.size(36.dp)
                )
            }
        }
    )
}

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@OptIn(ExperimentalMaterial3Api::class)
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    name = "DefaultPreviewDark"
)
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    name = "DefaultPreviewLight"
)
@Composable
fun UserBudgetsTopBarPreview() {
    BudgetBuddyTheme {
        Scaffold(
            Modifier.fillMaxSize(),
            topBar = { UserBudgetsTopBar(User("", "User"), {}) },
        ) {}
    }
}


/***************************
 * BudgetScreen Top Bar
 ***************************/
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun BudgetTopBar(
    budget: Budget,
    hideSettingsButton: Boolean = false,
    onSettingsClick: () -> Unit = {}
) {
    TopBarTemplate(
        title = { BarTitle(budget.name) },
        actions = {
            if (!hideSettingsButton)
                IconButton(onClick = onSettingsClick) {
                    Icon(
                        Icons.Filled.Settings,
                        contentDescription = "Manage budget"
                    )
                }
        }
    )
}

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@OptIn(ExperimentalMaterial3Api::class)
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    name = "DefaultPreviewDark"
)
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    name = "DefaultPreviewLight"
)
@Composable
fun BudgetTopBarPreview() {
    BudgetBuddyTheme {
        Scaffold(
            Modifier.fillMaxSize(),
            topBar = { BudgetTopBar(Budget("", "Budget name")) },
        ) {}
    }
}