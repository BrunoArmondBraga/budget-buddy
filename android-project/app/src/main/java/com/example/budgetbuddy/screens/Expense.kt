package com.example.budgetbuddy.screens

import android.annotation.SuppressLint
import android.content.res.Configuration
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberTopAppBarState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.budgetbuddy.components.AddPaymentButton
import com.example.budgetbuddy.components.CategoryTopBar
import com.example.budgetbuddy.components.ReceiptButton
import com.example.budgetbuddy.components.UserPreview
import com.example.budgetbuddy.ui.theme.BudgetBuddyTheme
import com.example.core.Budget
import com.example.core.Category
import com.example.core.Expense
import com.example.core.PartialPayment
import com.example.core.User

@Composable
fun PaymentCard(
    user: User,
    text: String,
    value: Float,
    color: Color,
    button: @Composable () -> Unit
) {

    Surface (
        color = color,
        shape = RoundedCornerShape(16.dp),
        modifier = Modifier.fillMaxWidth()
    ){
        Row (
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.padding(12.dp)
        ) {
            Row (
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.spacedBy(4.dp),
            ) {
                UserPreview(user)
                Text(text = "$text R$ $value", style = MaterialTheme.typography.bodyLarge)
            }
            button()
        }
    }
}


@Composable
fun ExpenseVisualizer(
    expense: Expense,
    onEditExpenseClick: () -> Unit,
    onReceiptClick: (receipt: String) -> Unit
) {
    val surfaceColor = MaterialTheme.colorScheme.surfaceContainer
    Surface (
        color = surfaceColor,
        shape = RoundedCornerShape(16.dp),
        modifier = Modifier.fillMaxWidth()
    ) {
        Column (
            Modifier.padding(20.dp),
            verticalArrangement = Arrangement.spacedBy(12.dp)
        ) {
            Column (
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.spacedBy(12.dp)
            ) {
                Text(
                    text = expense.name,
                    textAlign = TextAlign.Center,
                    style = MaterialTheme.typography.headlineSmall
                )
                Text(
                    text = expense.description,
                    textAlign = TextAlign.Left,
                    style = MaterialTheme.typography.bodyLarge
                )
            }
            Row(
                horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier.fillMaxWidth()
            ) {
                Column {
                    Text(
                        text = "total",
                        style = MaterialTheme.typography.bodyLarge
                    )
                    Text(
                        text = expense.totalValue.toString(),
                        style = MaterialTheme.typography.displaySmall
                    )
                }
                Column {
                    Text(
                        text = "remaining",
                        style = MaterialTheme.typography.bodyLarge
                    )
                    Text(
                        text = expense.remainingValue.toString(),
                        style = MaterialTheme.typography.displaySmall
                    )
                }
            }
            Row(modifier = Modifier.fillMaxWidth()) {
                Text(text = "payed by ")
                UserPreview(user = expense.expensePayer)
            }
            Text(text = "on ${expense.expenseDate}")
            Row(
                horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier.fillMaxWidth()
            ) {
                ReceiptButton(onClick = { onReceiptClick(expense.receipt) })
                IconButton(onClick = onEditExpenseClick) {
                    Icon(
                        Icons.Filled.Edit,
                        contentDescription = "Edit expense",
                        tint = MaterialTheme.colorScheme.primary
                    )
                }
            }


        }
    }
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ExpenseScreen(
    currentUser: User,
    currentBudget: Budget,
    currentCategory: Category,
    expense: Expense,
    partialPayments: List<PartialPayment>,
    onEditExpenseClick: () -> Unit,
    onReceiptClick: (receipt: String) -> Unit,
    onAddPartialPaymentClick: (expense: Expense) -> Unit,
    getDebt: (expense: Expense, user: User) -> Float
) {
    val scrollBehavior = TopAppBarDefaults.pinnedScrollBehavior(rememberTopAppBarState())

    Scaffold(
        Modifier
            .fillMaxSize()
            .nestedScroll(scrollBehavior.nestedScrollConnection),

        topBar = { CategoryTopBar(currentBudget, currentCategory, true) },
    ) { innerPadding ->
        LazyColumn (
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy(14.dp),
            modifier = Modifier
                .fillMaxWidth()
                .padding(innerPadding)
                .padding(horizontal = 36.dp)
        ) {
            item {
                ExpenseVisualizer(expense, onEditExpenseClick, onReceiptClick)
            }
            item{
                Text(text = "Payments", style = MaterialTheme.typography.headlineSmall)
            }
            items(partialPayments) {
                partialPayment -> PaymentCard(
                    user = partialPayment.payer,
                    text = "paid",
                    value = partialPayment.value,
                    color = MaterialTheme.colorScheme.surfaceTint,
                    button = { ReceiptButton(onClick = { onReceiptClick(partialPayment.receipt) }) }
                )
            }
            item {
                Text(text = "Remaining debtors", style = MaterialTheme.typography.headlineSmall)
            }
            items(expense.debtors) {
                debtor -> PaymentCard(
                    user = debtor,
                    text = "owes",
                    value = getDebt(expense, currentUser),
                    color = MaterialTheme.colorScheme.errorContainer,
                    button = {
                        if (currentUser == debtor)
                            AddPaymentButton(onClick = { onAddPartialPaymentClick(expense) })
                    }
                )
            }
        }
    }
}

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    name = "DefaultPreviewDark"
)
@Preview(
    uiMode = Configuration.UI_MODE_NIGHT_NO,
    name = "DefaultPreviewLight"
)
@Composable
fun ExpenseScreenPreview() {
    BudgetBuddyTheme {
        ExpenseScreen(
            currentUser = User("","Debtor"),
            currentBudget = Budget("", "Budget name"),
            currentCategory = Category("Category name", "Star"),
            expense = Expense(
                name ="Expense name",
                description = "This is a fake expense. Don't pay it! This is a very long description, with lots of words.",
                expensePayer = User("", "Payer"),
                expenseDate = "10/02/2028",
                totalValue = 20f,
                remainingValue = 75f,
                debtors = listOf(
                    User("", "Debtor"),
                    User("", "Other Debtor"),
                ),
                receipt = "",
            ),
            partialPayments = listOf(
                PartialPayment(User("", "Payer"), 10f, receipt = "",),
                PartialPayment(User("", "Other Payer"), 10f, receipt = "",),
            ),
            onEditExpenseClick = {},
            onReceiptClick = {},
            onAddPartialPaymentClick = {},
            getDebt = { _, _ -> 1f },
        )
    }
}