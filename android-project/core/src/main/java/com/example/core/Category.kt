package com.example.core

import kotlinx.serialization.Serializable

@Serializable
data class Category(val name: String, val icon: String = "")