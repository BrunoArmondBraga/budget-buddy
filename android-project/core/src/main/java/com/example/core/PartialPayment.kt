package com.example.core

import kotlinx.serialization.Serializable

@Serializable
data class PartialPayment(val payer: User, val value: Float, val receipt: String = "")