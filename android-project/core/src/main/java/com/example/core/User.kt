package com.example.core

import kotlinx.serialization.Serializable

@Serializable
data class User(val username: String, val name: String)