package com.example.core

import kotlinx.serialization.Serializable
import java.util.Date
import kotlin.math.exp

@Serializable
data class ExpenseRequest(val name: String, val description: String,
                          val expensePayer: User, val expenseDate: String, val totalValue: String,
                          val debtors: List<User>)
@Serializable
data class Expense(val name: String, val description: String,
                   val expensePayer: User, val expenseDate: String, val totalValue: Float,
                   val remainingValue: Float, val debtors: List<User>, val receipt: String = ""){
    fun toExpenseRequest(): ExpenseRequest{
        return ExpenseRequest(
            name = name,
            description = description,
            expensePayer = expensePayer,
            expenseDate = expenseDate,
            totalValue = totalValue.toString(),
            debtors = debtors
        )
    }
}