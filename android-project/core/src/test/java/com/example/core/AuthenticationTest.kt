package com.example.core

import org.junit.Test

import org.junit.Assert.*

class AuthenticationTest {
    @Test
    fun invalidUser() {
        val aut = Authentication()
        assertEquals(null, aut.validate("", ""))
    }
}
