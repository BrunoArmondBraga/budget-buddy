package com.example.core

import org.junit.Test

import org.junit.Assert.*

import java.util.Date

class CategoryTest {

    @Test
    fun testAddExpense() {
        val singleton: CommunityBudget = CommunityBudget("")
        val category: Category = Category("", "", singleton)

        val expense: Expense = Expense("e", "", 0f,
            category, "", Date())

        category.addExpense(expense)
        assertEquals(category.expenses.contains(expense), true)

        val otherExpense: Expense = Expense("e", "", 0f,
            category, "", Date())

        category.addExpense(otherExpense)
        assertEquals(category.expenses.contains(otherExpense), true)
    }

    @Test
    fun testAddSharedExpense() {
        val singleton: CommunityBudget = CommunityBudget("")
        val category: Category = Category("", "", singleton)

        val user: LocalUser = LocalUser("user", "")
        val otherUser: LocalUser = LocalUser("otherUser", "")
        val thirdUser: LocalUser = LocalUser("thirdUser", "")
        val nonDebtor: LocalUser = LocalUser("nonDebtor", "")

        singleton.addUser(user)
        singleton.addUser(otherUser)
        singleton.addUser(thirdUser)
        singleton.addUser(nonDebtor)

        val partialDebt: Float = 3f / 3
        val shared: SharedExpense = SharedExpense(user, mutableListOf(otherUser, thirdUser),
            singleton, "", "", 3f, category, "", Date())

        category.addExpense(shared)
        assertEquals(category.expenses.contains(shared), true)
        assertEquals(singleton.getDebt(otherUser, user), partialDebt)
        assertEquals(singleton.getDebt(thirdUser, user), partialDebt)
        assertEquals(singleton.getDebt(nonDebtor, user), 0f)
    }

    @Test
    fun testRemoveExpense() {
        val singleton: CommunityBudget = CommunityBudget("")
        val category: Category = Category("", "", singleton)

        val expense: Expense = Expense("e", "", 0f,
            category, "", Date())
        category.addExpense(expense)

        val otherExpense: Expense = Expense("e", "", 0f,
            category, "", Date())
        category.addExpense(otherExpense)

        category.removeExpense(expense)
        assertEquals(category.expenses.contains(expense), false)

        category.removeExpense(otherExpense)
        assertEquals(category.expenses.contains(otherExpense), false)
    }

    @Test
    fun testRemoveSharedExpense() {
        val singleton: CommunityBudget = CommunityBudget("")
    }
}