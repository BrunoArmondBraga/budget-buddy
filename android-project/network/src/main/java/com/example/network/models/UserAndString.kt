package com.example.network.models

import kotlinx.serialization.Serializable

@Serializable
class UserAndString (
    val username: String,
    val name: String,
    val extra: String
)

@Serializable
class TwoStrings(
    val first: String,
    val second: String
)