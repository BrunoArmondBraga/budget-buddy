package com.example.network.models

import com.example.core.Budget
import com.example.core.Category
import com.example.core.Expense
import com.example.core.ExpenseRequest
import com.example.core.PartialPayment
import com.example.core.User
import kotlinx.serialization.Serializable

@Serializable
class UserStringWrapper (
    val user: User,
    val newName: String
)

@Serializable
class UserBudgetWrapper (
    val user: User,
    val budget: Budget
)

@Serializable
class BudgetCategoryWrapper (
    val budget: Budget,
    val category: Category
)

@Serializable
class BudgetCategoryExpenseWrapper (
    val budget: Budget,
    val category: Category,
    val expense: ExpenseRequest
)

@Serializable
class ExpensePartialPaymentWrapper (
    val expense: ExpenseRequest,
    val partialPayment: PartialPayment
)

@Serializable
class EditCategoryRequest (
    val budgetId: String,
    val oldName: String,
    val oldIcon: String,
    val newName: String,
    val newIcon: String
)

@Serializable
class EditExpenseRequest(
    val budget: Budget,
    val category: Category,
    val expense: ExpenseRequest,
    val newName: String,
    val newDescription: String,
    val newDate: String,
    val newReceipt: String
)