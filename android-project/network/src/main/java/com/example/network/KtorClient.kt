package com.network

import com.example.core.Budget
import com.example.core.Category
import com.example.core.Expense
import com.example.core.PartialPayment
import com.example.core.User
import com.example.network.models.BudgetCategoryExpenseWrapper
import com.example.network.models.BudgetCategoryWrapper
import com.example.network.models.EditCategoryRequest
import com.example.network.models.EditExpenseRequest
import com.example.network.models.ExpensePartialPaymentWrapper
import com.example.network.models.TwoStrings
import com.example.network.models.UserBudgetWrapper
import com.example.network.models.UserInfo
import com.example.network.models.UserRequest
import com.example.network.models.UserStringWrapper
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.engine.okhttp.OkHttp
import io.ktor.client.plugins.DefaultRequest
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.logging.Logger
import io.ktor.client.plugins.logging.Logging
import io.ktor.client.plugins.logging.SIMPLE
import io.ktor.client.request.get
import io.ktor.client.request.parameter
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import io.ktor.client.statement.HttpResponse
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.contentType
import io.ktor.serialization.kotlinx.json.json
import kotlinx.serialization.json.Json

class KtorClient() {
    private var ip: String = "localhost"
    private val client = HttpClient(OkHttp){

        install(Logging){
            logger = Logger.SIMPLE
        }

        install(ContentNegotiation){
            json(Json {
                ignoreUnknownKeys = true
            })
        }
        install(DefaultRequest) {
            contentType(ContentType.Application.Json)
        }
    }

    fun setIP(newIP: String) {
        ip = newIP
    }

    suspend fun testIP(): Boolean {
        return try{
            val response = client.get("http://${ip}:8080/true")
            response.status == HttpStatusCode.OK
        } catch (e: Exception){
            false
        }
    }

    // Authentication functions
    suspend fun verifyLogin(username: String, password: String): User? {
        return try {
            val response = client.get("http://${ip}:8080/authenticateUser") {
                parameter("username", username)
                parameter("password", password)
            }
            response.body<User>()
        } catch (e: Exception) {
            null
        }
    }

    // User functions
    suspend fun createUser(username: String, name: String, password: String): Boolean {
        val postMessage = UserInfo(username, name, password)

        val response = client.post("http://${ip}:8080/users/save") {
            setBody(postMessage)
        }
        /* TODO: Fix return */
        return true
    }

    suspend fun editProfile(user: User, newName: String): User {
        val request = UserStringWrapper(user, newName)

        val response = client.post("http://${ip}:8080/users/edit") {
            setBody(request)
        }

        return User(user.username, newName)
    }

    suspend fun getUserBudgets(u: User): List<Budget> {
        return try {
            val response: HttpResponse = client.get("http://${ip}:8080/budgetUsers/user") {
                parameter("username", u.username)
            }
            response.body<List<Budget>>()
            //val finalList : List<Budget> = response.body()
            //finalList
        } catch (e: Exception) {
            val nullList: List<Budget> = listOf()
            nullList
        }
    }


    // Budget modification functions
    suspend fun createBudget(u: User, b: Budget) {
        val request = UserBudgetWrapper(u, b)

        val response = client.post("http://${ip}:8080/createBudget") {
            setBody(request)
        }
    }

    suspend fun editBudget(b: Budget, name: String): Budget {
        val request = TwoStrings(b.id, name)

        client.post("http://${ip}:8080/budgets/changeName") {
            setBody(request)
        }

        return Budget(b.id, name)
    }

    // Budget users functions
    suspend fun addBudgetUser(b: Budget, u: User) {
        val request = UserBudgetWrapper(u, b)

        val response = client.post("http://${ip}:8080/budgetUsers/save") {
            setBody(request)
        }
    }
    suspend fun removeBudgetUser(b: Budget, u: User) {
        val request = UserBudgetWrapper(u, b)

        val response = client.post("http://${ip}:8080/budgetUsers/delete") {
            setBody(request)
        }
    }

    suspend fun getBudgetUsers(b: Budget): List<User> {
        return try{
            val response = client.get("http://${ip}:8080/budgetUsers/budget") {
                parameter("id", b.id)
            }
            response.body<List<User>>()
        } catch (e: Exception) {
            val nullList: List<User> = listOf()
            nullList
        }
    }

    // Categories functions
    suspend fun getBudgetCategories(b: Budget): List<Category> {
        return try {
            val response = client.get("http://${ip}:8080/categories/byBudget") {
                parameter("id", b.id)
            }
            response.body<List<Category>>()
        } catch (e: Exception) {
            val nullList: List<Category> = listOf()
            nullList
        }
    }

    suspend fun addCategory(b: Budget, c: Category) {
        val request = BudgetCategoryWrapper(b, c)

        val response = client.post("http://${ip}:8080/categories/add") {
            setBody(request)
        }
    }

    suspend fun getUnusedBudgetId(): String{
        return client.get("http://${ip}:8080/nextBudgetId").body<Int>().toString()
    }

    suspend fun getBudget(id: String): Budget{
        return client.get("http://${ip}:8080/getbudget/byId"){
            parameter("id", id)
        }.body()
    }

    suspend fun editCategory(b: Budget, c: Category, newName: String, newIcon: String): Category {
        val request = EditCategoryRequest(b.id, c.name, c.icon, newName, newIcon)

        client.post("http://${ip}:8080/categories/edit"){
            setBody(request)
        }

        return Category(newName, newIcon)
    }

    suspend fun removeCategory(b: Budget, c: Category) {
        val wrapper = BudgetCategoryWrapper(b, c)

        client.post("http://${ip}:8080/categories/remove"){
            setBody(wrapper)
        }
    }

    // Expenses functions
    suspend fun getAllExpenses(b: Budget): Expense? {
        return client.get("http://${ip}:8080/expenses/budget"){
            setBody(b)
        }.body<Expense>()
    }

    suspend fun getCategoryExpenses(b: Budget, c: Category): List<Expense> {
        return try {

            val response = client.get("http://${ip}:8080/categories/expenses") {
                parameter("budgetId", b.id)
                parameter("categoryName", c.name)
            }
            response.body<List<Expense>>()
        } catch (e: Exception) {
            val nullList: List<Expense> = listOf()
            nullList
        }
    }

    suspend fun addExpense(b: Budget, c: Category, e: Expense) {
        val request = BudgetCategoryExpenseWrapper(b, c, e.toExpenseRequest())

        val response = client.post("http://${ip}:8080/expenses/save") {
            setBody(request)
        }
    }

    suspend fun removeExpense(b: Budget, c: Category, e: Expense) {
        val request = BudgetCategoryExpenseWrapper(b, c, e.toExpenseRequest())

        client.post("http://${ip}:8080/expenses/remove") {
            setBody(request)
        }
    }

    suspend fun editExpense(b: Budget, c: Category, e: Expense, newName: String,
                            newDescription: String, newDate: String, newReceipt: String): Expense {
        val request = EditExpenseRequest(b, c, e.toExpenseRequest(), newName, newDescription, newDate, newReceipt)

        client.post("http://${ip}:8080/expenses/edit") {
            setBody(request)
        }

        return Expense(newName, newDescription, e.expensePayer, newDate, e.totalValue,
            e.remainingValue, e.debtors, newReceipt)
    }

    // Partial Payment functions
    suspend fun addPartialPayment(e: Expense, p: PartialPayment): Expense {
        val request = ExpensePartialPaymentWrapper(e.toExpenseRequest(), p)

        client.post("http://${ip}:8080/partialPayment/add") {
            setBody(request)
        }

        return Expense(
            e.name,
            e.description,
            e.expensePayer,
            e.expenseDate,
            e.totalValue,
            e.remainingValue - ((e.remainingValue) / e.debtors.size),
            e.debtors.filter { it != p.payer },
            e.receipt
        )
    }

    suspend fun removePartialPayment(e: Expense, p: PartialPayment) {
        val request = ExpensePartialPaymentWrapper(e.toExpenseRequest(), p)

        client.post("http://${ip}:8080/partialPayment/remove") {
            setBody(request)
        }
    }

    suspend fun getPartialPayments(e: Expense): List<PartialPayment> {
        return try{
            val response = client.get("http://${ip}:8080/partialPayment/byExpense") {
                parameter("name", e.name)
                parameter("description", e.description)
                parameter("expenseDate", e.expenseDate)
                parameter("totalValue", e.totalValue)
            }
            response.body<List<PartialPayment>>()
        } catch (e: Exception) {
            val nullList: List<PartialPayment> = listOf()
            nullList
        }
    }
}