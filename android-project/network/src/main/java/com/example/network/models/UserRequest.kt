package com.example.network.models

import com.example.core.Budget
import com.example.core.User
import kotlinx.serialization.Serializable

@Serializable
class UserRequest (
    val username: String,
    val password: String
)

@Serializable
class UserInfo (
    val username: String,
    val name: String,
    val password: String
)