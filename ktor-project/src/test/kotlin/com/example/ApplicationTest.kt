package com.example

import com.example.plugins.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.server.routing.get
import io.ktor.server.testing.*
import kotlin.test.*

class ApplicationTest {
    @Test
    fun testTrueRoute() = testApplication {
        application {
            configureRouting()
        }
        client.get("/true").apply {
            assertEquals(HttpStatusCode.OK, status)
        }

        val response = client.get("/nextBudgetId").apply {
            assertEquals(HttpStatusCode.OK, status)
        }

        assertNotNull(response)
    }

    @Test
    fun testNextBudgetIdRoute() = testApplication {
        application {
            configureRouting()
        }

        val response = client.get("/nextBudgetId")

        assertNotNull(response)
    }

    @Test
    fun testGetCategoriesRoute() = testApplication {
        application {
            configureRouting()
        }

        val response = client.get("/categories").apply {
            assertEquals(HttpStatusCode.OK, status)
        }
    }

    @Test
    fun testGetExpensesRoute() = testApplication {
        application {
            configureRouting()
        }

        val response = client.get("/expenses").apply {
            assertEquals(HttpStatusCode.OK, status)
        }
    }

    @Test
    fun testGetDebtorsRoute() = testApplication {
        application {
            configureRouting()
        }

        val response = client.get("/debtors").apply {
            assertEquals(HttpStatusCode.OK, status)
        }
    }

    @Test
    fun testGetBudgetsRoute() = testApplication {
        application {
            configureRouting()
        }

        val response = client.get ("/budgets").apply {
            assertEquals(HttpStatusCode.OK, status)
        }
    }

    @Test
    fun testDebtorsRoute() = testApplication {
        application {
            configureRouting()
        }

        client.get("/debtors").apply {
            assertEquals(HttpStatusCode.OK, status)
        }
    }

    @Test
    fun testNullParametersPartialPaymentByExpenseRoute() = testApplication {
        application {
            configureRouting()
        }

        client.get("/partialPayment/byExpense") {
            parameter("name", null)
            parameter("description", "Dinner")
            parameter("expenseDate", "10/07/2023")
            parameter("totalValue", 250.0f)
        }.apply {
            assertEquals(HttpStatusCode.BadRequest, status)
        }

        client.get("/partialPayment/byExpense") {
            parameter("name", "bruno")
            parameter("description", null)
            parameter("expenseDate", "10/07/2023")
            parameter("totalValue", 250.0f)
        }.apply {
            assertEquals(HttpStatusCode.BadRequest, status)
        }

        client.get("/partialPayment/byExpense") {
            parameter("name", "bruno")
            parameter("description", "Dinner")
            parameter("expenseDate", null)
            parameter("totalValue", 250.0f)
        }.apply {
            assertEquals(HttpStatusCode.BadRequest, status)
        }

        client.get("/partialPayment/byExpense") {
            parameter("name", "bruno")
            parameter("description", "Dinner")
            parameter("expenseDate", "10/07/2023")
            parameter("totalValue", null)
        }.apply {
            assertEquals(HttpStatusCode.BadRequest, status)
        }
    }

    @Test
    fun testGetExpenseDebtorsRoute() = testApplication {
        application {
            configureRouting()
        }

        client.get("/expense/debtors") {
            parameter("id", null)
        }.apply {
            assertEquals(HttpStatusCode.BadRequest, status)
        }
    }

    @Test
    fun testCategoriesExpensesRoute() = testApplication {
        application {
            configureRouting()
        }

        client.get("/categories/expenses") {
            parameter("budgetId", null)
            parameter("categoryName", "First Category")
        }.apply {
            assertEquals(HttpStatusCode.BadRequest, status)
        }

        client.get("/categories/expenses") {
            parameter("budgetId", "1")
            parameter("categoryName", null)
        }.apply {
            assertEquals(HttpStatusCode.BadRequest, status)
        }
    }

    @Test
    fun testCategoriesByBudgetRoute() = testApplication {
        application {
            configureRouting()
        }

        client.get("/categories/byBudget") {
            parameter("id", null)
        }.apply {
            assertEquals(HttpStatusCode.BadRequest, status)
        }
    }

    @Test
    fun testBudgetUserUserRoute() = testApplication {
        application {
            configureRouting()
        }

        client.get("/budgetUsers/user") {
            parameter("username", null)
        }.apply {
            assertEquals(HttpStatusCode.BadRequest, status)
        }
    }

    @Test
    fun testBudgetByIdRoute() = testApplication {
        application {
            configureRouting()
        }

        client.get("/getbudget/byId") {
            parameter("id", null)
        }.apply {
            assertEquals(HttpStatusCode.BadRequest, status)
        }
    }

    @Test
    fun testBudgetUsersBudgetRoute() = testApplication {
        application {
            configureRouting()
        }

        client.get("/budgetUsers/budget") {
            parameter("id", null)
        }.apply {
            assertEquals(HttpStatusCode.BadRequest, status)
        }
    }

    @Test
    fun testAuthenticateUserRoute() = testApplication {
        application {
            configureRouting()
        }

        client.get("/authenticateUser") {
            parameter("username", null)
            parameter("password", "password")
        }.apply {
            assertEquals(HttpStatusCode.BadRequest, status)
        }

        client.get("/authenticateUser") {
            parameter("username", "username")
            parameter("password", null)
        }.apply {
            assertEquals(HttpStatusCode.BadRequest, status)
        }
    }
}
