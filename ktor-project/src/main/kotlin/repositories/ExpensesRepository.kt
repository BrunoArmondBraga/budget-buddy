package repositories

import database.ExpenseDao
import dto.CategoryRequest
import dto.EditExpenseRequest
import dto.ExpenseRequest
import models.Category
import models.Expense
import kotlin.math.exp

class ExpensesRepository(
    private val dao: ExpenseDao = ExpenseDao()
) {
    suspend fun expenses() = dao.findAllExpenses()

    suspend fun save(expense: Expense, numberOfDebtors: Int): Int {
        return dao.addExpense(expense, numberOfDebtors)
    }

    suspend fun delete(expense: Expense) {
        dao.deleteExpense(expense)
    }

    suspend fun editExpense(expense: EditExpenseRequest){
        dao.editExpense(expense)
    }

    suspend fun findExpensesByBudgetId(budgetId: Int): List<Expense> {
        return dao.findExpensesByBudgetId(budgetId)
    }

    suspend fun findExpensesByCategoryName(budgetId: Int, categoryName: String): List<Expense> {
        return dao.findExpensesByCategoryName(budgetId, categoryName)
    }

    suspend fun getIdByExpense(expense: Expense): Int? {
        return dao.getIdByExpense(expense)
    }

    suspend fun getIdByExpenseRequest(name: String,
                                      description: String,
                                      expenseDate: String,
                                      totalValue: String): Int? {
        return dao.getIdByExpenseRequest(name, description, expenseDate, totalValue)
    }

    suspend fun editCategoryName(budgetId: Int, oldName: String, newName: String){
        dao.editCategoryName(budgetId, oldName, newName)
    }

    suspend fun updateRemaining(expenseId: Int, value: Float){
        dao.updateRemaining(expenseId, value)
    }
}