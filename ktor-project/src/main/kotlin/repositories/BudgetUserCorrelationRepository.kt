package repositories

import database.BudgetUserCorrelationDao
import dto.UserResponse
import models.Budget


class BudgetUserCorrelationRepository (
    private val dao: BudgetUserCorrelationDao = BudgetUserCorrelationDao()
) {
    suspend fun correlations() = dao.findAll()


    suspend fun addUserToBudget(budgetId: Int, username: String) {
        dao.addUserToBudget(budgetId, username)
    }

    suspend fun removeUserFromBudget(budgetId: Int, username: String) {
        dao.removeUserFromBudget(budgetId, username)
    }

    suspend fun findUsersByBudget(budgetId: Int): List<UserResponse> {
        return dao.findUsersByBudget(budgetId)
    }

    suspend fun findBudgetsByUser(username: String): List<Budget> {
        return dao.findBudgetsByUser(username)
    }
}