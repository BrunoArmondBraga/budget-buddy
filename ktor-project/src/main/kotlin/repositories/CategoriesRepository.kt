package repositories

import database.CategoryDao
import dto.CategoryRequest
import models.Budget
import models.Category

class CategoriesRepository (
    private val dao: CategoryDao = CategoryDao()
) {
    suspend fun categories() = dao.findAll()

    suspend fun addCategory(b: Budget, c: CategoryRequest){
        dao.createCategory(b.id.toInt(), c)
    }

    suspend fun removeCategory(b: Budget, c: CategoryRequest){
        dao.removeCategory(b.id.toInt(), c)
    }

    suspend fun editCategory(budgetId: String, oldName: String, oldIcon: String, newName: String, newIcon: String){
        dao.editCategory(budgetId, oldName, oldIcon, newName, newIcon)
    }

    suspend fun findCategoriesByBudget(id: Int): List<Category> {
        return dao.getAllCategories(id)
    }

}