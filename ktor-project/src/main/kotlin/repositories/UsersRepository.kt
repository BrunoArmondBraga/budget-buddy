package repositories

import database.UserDao
import dto.UserRequest
import dto.UserRequestWithoutName
import dto.UserRequestWithoutPassword
import models.User

class UsersRepository(
    private val dao: UserDao = UserDao()
) {
    suspend fun users() = dao.findAllUsers()

    suspend fun getUserByUsername(username: String): User? {
        return dao.getUserByUsername(username)
    }

    suspend fun authenticate(username: String, password: String): User? {
        return dao.authenticateUser(username, password)
    }

    suspend fun save(userRequest: UserRequest){
        dao.saveUser(userRequest)
    }

    suspend fun delete(name: String): Int {
        return dao.deleteUser(name)
    }

    suspend fun transformUsernamesToUsers(usernames: List<String>): List<User> {
        return usernames.mapNotNull { username ->
            getUserByUsername(username)
        }
    }

    suspend fun editName(username: String, newName: String){
        dao.editName(username, newName)
    }

    suspend fun getNameByUsername(username: String): String?{
        return dao.getNameByUsername(username)
    }
}