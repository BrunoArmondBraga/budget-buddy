package repositories

import database.BudgetDao

class BudgetsRepository(
    private val dao: BudgetDao = BudgetDao()
) {
    suspend fun budgets() = dao.findAll()

    suspend fun createBudget(name: String, creator: String): Int? {
        return dao.createBudget(name, creator)
    }

    suspend fun findBudgetNameById(budgetId: Int): String? {
        return dao.findBudgetNameById(budgetId)
    }

    suspend fun getNextId(): Int{
        return dao.getNextId()
    }

    suspend fun changeName(budgetId: Int, newName: String) {
        dao.changeBudgetName(budgetId, newName)
    }
}
