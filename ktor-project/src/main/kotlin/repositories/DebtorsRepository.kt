package repositories

import database.DebtorDao
import models.Debtor

class DebtorsRepository(
    private val dao: DebtorDao = DebtorDao()
) {
    suspend fun debtors() = dao.findAll()

    suspend fun saveDebtor(debtor: Debtor) {
        return dao.saveDebtor(debtor)
    }

    suspend fun removeDebtor(debtor: Debtor) {
        dao.removeDebtor(debtor)
    }

    suspend fun getDebtorByExpenseId(expenseId: Int): List<String> {
        return dao.getDebtorByExpenseId(expenseId)
    }

    suspend fun getUnpayedDebtorByExpenseId(expenseId: Int): List<String> {
        return dao.getUnpayedDebtorByExpenseId(expenseId)
    }

    suspend fun addPayment(expenseId: Int, username: String){
        dao.addPayment(expenseId, username)
    }

    suspend fun removePayment(expenseId: Int, username: String){
        dao.removePayment(expenseId, username)
    }

    suspend fun getUsernamesOfPartialPayers(expenseId: Int): List<Pair<String, Float>> {
        return dao.getPayedPayments(expenseId)
    }
}