package models

import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Table
import java.util.UUID
@Serializable
class Expense (
    val id: Int,
    val budgetId: Int,
    val categoryName: String,
    val name: String,
    val description: String,
    val expensePayer: String,
    val totalValue: Float,
    val remainingValue: Float,
    val expenseDate: String
)

object Expenses : Table() {
    val id = integer("id").autoIncrement()
    val budgetId = integer("budgetId")
    val categoryName = text("categoryId")
    val name = text("name")
    val description = text("description")
    val expensePayer = text("payerUsername")
    val totalValue = float("totalValue")
    val remainingValue = float("reamainingValue")
    val expenseDate = text("expenseDate")

    override val primaryKey = PrimaryKey(id)
}
