package models

import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Table

@Serializable
class Category(
    val name: String,
    val icon: String
)

object Categories : Table(){
    val name = text("name")
    val budgetId = integer("idBudget")
    val icon = text("icon")

    override val primaryKey = PrimaryKey(name, budgetId)
}