package models

import org.jetbrains.exposed.sql.Table

class BudgetUserCorrelationTemplate(
    val budgetId: Int,
    val username: String
)

object BudgetUserCorrelation : Table(){
    val budgetId = integer("budgetId")
    val username = text("username")
}