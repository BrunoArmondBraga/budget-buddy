package models

import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Table

@Serializable
class User (
    val username: String,
    val name: String,
    val password: String
)

object Users : Table() {
    val username = text("username")
    val name = text("name")
    val password = text("password")

    override val primaryKey = PrimaryKey(username)
}