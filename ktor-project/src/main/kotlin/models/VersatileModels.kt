package models

import dto.CategoryRequest
import dto.ExpenseRequest
import dto.PartialPayment
import dto.UserRequestWithoutPassword
import kotlinx.serialization.Serializable

@Serializable
data class StringRequest(val name: String)

@Serializable
data class UserBudgetWrapper(
    val user: UserRequestWithoutPassword,
    val budget: Budget
)

@Serializable
data class BudgetCategoryWrapper(
    val budget: Budget,
    val category: CategoryRequest
)

@Serializable
data class BudgetCategoryExpenseWrapper(
    val budget: Budget,
    val category: Category,
    val expense: ExpenseRequest
)

@Serializable
data class ExpensePartialPaymentWrapper(
    val partialPayment: PartialPayment,
    val expense: ExpenseRequest
)

@Serializable
class UserStringWrapper (
    val user: UserRequestWithoutPassword,
    val newName: String
)