package models

import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Table

@Serializable
class Debtor (
    val expenseId: String,
    val username: String,
    val payed: Boolean,
    val value: Float
)

object Debtors : Table() {
    val expenseId = integer("idBudget")
    val username = text("username")
    val payed = bool("payed")
    val value = float("value")

    override val primaryKey = PrimaryKey(expenseId, username)
}