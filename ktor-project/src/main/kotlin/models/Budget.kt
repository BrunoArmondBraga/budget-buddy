package models

import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Table
import java.util.UUID
@Serializable
data class Budget (
    val id: String,
    val name: String
)

@Serializable
class BudgetAndUser(
    val budgetId: String,
    val budgetName: String,
    val username: String,
    val name: String,
)

object Budgets : Table() {
    val id = integer("id").autoIncrement()
    val name = text("name")

    override val primaryKey = PrimaryKey(id)
}
