package com.example.plugins


import dto.EditCategoryRequest
import dto.EditExpenseRequest
import dto.ExpenseResponse
import dto.PartialPayment
import dto.UserRequest
import dto.UserRequestWithoutPassword
import dto.UserResponse
import dto.toBudgetCorrelationResponse
import dto.toBudgetResponse
import dto.toExpenseResponse
import dto.toUserResponse
import io.ktor.http.HttpStatusCode
import io.ktor.server.application.*
import io.ktor.server.request.receive
import io.ktor.server.response.*
import io.ktor.server.routing.*
import models.Budget
import models.BudgetCategoryExpenseWrapper
import models.BudgetCategoryWrapper
import models.Debtor
import models.ExpensePartialPaymentWrapper
import models.StringRequest
import models.UserBudgetWrapper
import models.UserStringWrapper
import repositories.BudgetUserCorrelationRepository
import repositories.BudgetsRepository
import repositories.CategoriesRepository
import repositories.DebtorsRepository
import repositories.ExpensesRepository
import repositories.UsersRepository

fun Application.configureRouting() {

    val expensesRepository = ExpensesRepository()
    val usersRepository = UsersRepository()
    val budgetsRepository = BudgetsRepository()
    val correlationRepository = BudgetUserCorrelationRepository()
    val debtorsRepository = DebtorsRepository()
    val categoriesRepository = CategoriesRepository()

    routing {
        /* All Requests:
        Users Requests
        - get(/users)                               ->  Get all users
        - post(/saveUser) (name, password)          ->  Save new user
        - delete(/user)   (name)                    ->  Delete user by name
        - post(/authenticateUser) (name, password)  ->  Authenticate login
        */


        // Test IP request
        get("/true"){
            call.respond(HttpStatusCode.OK)
        }

        // Users Requests --------------------------------------------------------------------------

        // Get all users
        get ("/users"){
            val response = usersRepository.users().map {
                it.toUserResponse()
            }
            call.respond(response)
        }

        // Save new User
        post("/users/save"){
            val user = call.receive<UserRequest>()
            usersRepository.save(user).let {
                call.respondText (
                    "User ${user.name} created!",
                    status = HttpStatusCode.Created)
            }
        }

        // Edit existing User
        post("/users/edit"){
            val request = call.receive<UserStringWrapper>()
            val user = request.user

            usersRepository.editName(user.username, request.newName)

            call.respond(HttpStatusCode.OK)
        }

        delete("/user") {
            val user = call.receive<StringRequest>()
            val userName = user.name
            val numberOfDeleted = usersRepository.delete(userName)
            if(numberOfDeleted > 0){
                call.respondText("User with name '${user.name}' removed successfully!")
            }
            else {
                call.respondText("Error at removing User '${user.name}'!")
            }
        }

        get("/authenticateUser"){
            val username = call.parameters["username"]
            val password = call.parameters["password"]

            if(username == null || password == null){
                call.respond(HttpStatusCode.BadRequest)
            }
            else {
                val userExists = usersRepository.authenticate(username, password)
                if (userExists != null) {
                    call.respond(status = HttpStatusCode.OK, userExists.toUserResponse())
                } else {
                    call.respond(status = HttpStatusCode.BadRequest, ({ "" }))
                }
            }
        }

        // Budget-User correlation -----------------------------------------------------------------

        // Save the correlation between an User and a Budget
        post("/budgetUsers/save") {
            val request = call.receive<UserBudgetWrapper>()


            val budget = request.budget
            val user = request.user


            correlationRepository.addUserToBudget(budget.id.toInt(), user.username).let {
                call.respondText(
                    "User ${user.name} was added to Budget ${budget.name}",
                    status = HttpStatusCode.Created
                )
            }
        }

        // Delete the correlation between an User and a Budget
        post("/budgetUsers/delete") {
            val request = call.receive<UserBudgetWrapper>()


            val budget = request.budget
            val user = request.user


            correlationRepository.removeUserFromBudget(budget.id.toInt(), user.username).let {
                call.respondText(
                    "User ${user.name} was removed from Budget ${budget.name}",
                    status = HttpStatusCode.Created
                )
            }
        }

        // Get all Users correlated to a Budget
        get("/budgetUsers/budget"){
            val id = call.parameters["id"]

            if(id == null){
                call.respond(HttpStatusCode.BadRequest)
            }
            else{
                val response = correlationRepository.findUsersByBudget(id.toInt())

                call.respond(response)
            }
        }

        // Get all Budgets correlated to an User
        get("/budgetUsers/user"){
            val username = call.parameters["username"]

            if(username == null){
                call.respond(HttpStatusCode.BadRequest)
            }
            else {
                val response = correlationRepository.findBudgetsByUser(username)

                call.respond(response)
            }
        }

        // Get all correlations
        get("/budgetUsers/debug"){
            val response = correlationRepository.correlations().map {
                it.toBudgetCorrelationResponse()
            }
            call.respond(response)
        }

        // Budgets functions -----------------------------------------------------------------------

        // Get ALL Budgets
        get ("/budgets"){
            val response = budgetsRepository.budgets().map {
                it.toBudgetResponse()
            }
            call.respond(response)
        }

        // Create new Budget
        post("/createBudget"){

            val request = call.receive<UserBudgetWrapper>()
            val newIdBudget = budgetsRepository.getNextId()

            budgetsRepository.createBudget(request.budget.name, request.user.username)
            correlationRepository.addUserToBudget(newIdBudget, request.user.username)

            call.respond(HttpStatusCode.Created)
        }

        // Change the name of an existing Budget
        post("budgets/changeName"){
            val request = call.receive<Map<String, String>>()
            val values = request.values.toList()

            val budgetId = values[0].toInt()
            val newName = values[1]

            budgetsRepository.changeName(budgetId, newName)
            call.respond(HttpStatusCode.OK)
        }

        // Get a Budget by its Id
        get("/getbudget/byId"){
            val id = call.parameters["id"]

            if(id == null){
                call.respond(HttpStatusCode.BadRequest)
            }
            else {
                val name = budgetsRepository.findBudgetNameById(id.toInt())
                if (name != null) {
                    val budget = Budget(id, name)
                    call.respond(budget)
                }
            }
        }

        // Get the next vacant Id
        get("/nextBudgetId"){
            val response = budgetsRepository.getNextId()
            call.respond(response)
        }

        // Category functions ----------------------------------------------------------------------

        // Get all Categories
        get("/categories"){
            val response = categoriesRepository.categories()
            call.respond(response)
        }

        // Get all Categories of a Budget
        get("/categories/byBudget"){
            val id = call.parameters["id"]

            if(id == null){
                call.respond(HttpStatusCode.BadRequest)
            }
            else{
                val response = categoriesRepository.findCategoriesByBudget(id.toInt())
                call.respond(response)
            }
        }

        // Add a Category to a Budget
        post("/categories/add"){
            val request = call.receive<BudgetCategoryWrapper>()
            categoriesRepository.addCategory(request.budget, request.category)
            call.respond(HttpStatusCode.Created)
        }

        // Remove a Category from a Budget
        post("/categories/remove"){
            val request = call.receive<BudgetCategoryWrapper>()
            categoriesRepository.removeCategory(request.budget, request.category)
            call.respond(HttpStatusCode.OK)
        }

        // Edit parameters of a Category
        post("/categories/edit"){
            val request = call.receive<EditCategoryRequest>()
            categoriesRepository.editCategory(request.budgetId, request.oldName,
                request.oldIcon, request.newName, request.newIcon)

            expensesRepository.editCategoryName(request.budgetId.toInt(), request.oldName,
                request.newName)

            call.respond(HttpStatusCode.OK)
        }

        // Get all Expenses of a Category
        get("/categories/expenses"){
            val budgetId = call.parameters["budgetId"]
            val categoryName = call.parameters["categoryName"]

            if(budgetId == null || categoryName == null){
                call.respond(HttpStatusCode.BadRequest)
            }
            else {
                val expenseList =
                    expensesRepository.findExpensesByCategoryName(budgetId.toInt(), categoryName)

                val processedExpenses = mutableListOf<ExpenseResponse>()

                for (expense in expenseList) {
                    val userOfThisExpense = usersRepository.getUserByUsername(expense.expensePayer)
                    val debtors = debtorsRepository.getUnpayedDebtorByExpenseId(expense.id)

                    if (userOfThisExpense != null) {
                        val processedExpense = expense.toExpenseResponse(
                            userOfThisExpense,
                            usersRepository.transformUsernamesToUsers(debtors)
                        )
                        processedExpenses.add(processedExpense)
                    }
                }

                call.respond(processedExpenses)
            }
        }

        // expense functions -----------------------------------------------------------------------

        // Get all Expenses
        get("/expenses"){
            val response = expensesRepository.expenses()
            call.respond(response)
        }

        // Get all Expenses of a budget
        get("/expenses/budget"){
            val request = call.receive<Budget>()
            val response = expensesRepository.findExpensesByBudgetId(request.id.toInt())
            call.respond(response)
        }

        // Create a new expense
        post("/expenses/save"){
            val request = call.receive<BudgetCategoryExpenseWrapper>()
            val b = request.budget
            val c = request.category
            val e = request.expense

            val expense = e.toExpense(c, b)
            val numberOfDebtors = e.debtors.size

            val id = expensesRepository.save(expense, numberOfDebtors)

            if(id > 0){
                for(debtor in e.debtors){
                    val newDebtor = Debtor(id.toString(), debtor.username, false,
                        e.totalValue.toFloat()/(numberOfDebtors + 1))
                    debtorsRepository.saveDebtor(newDebtor)
                }
                call.respond(HttpStatusCode.OK)
            }
            else{
                call.respond(HttpStatusCode.Conflict)
            }
        }

        // Delete an existing expense
        post("/expenses/remove"){
            val request = call.receive<BudgetCategoryExpenseWrapper>()
            val b = request.budget
            val c = request.category
            val e = request.expense

            val expense = e.toExpense(c, b)
            val expenseId = expensesRepository.getIdByExpense(expense)
            expensesRepository.delete(expense)

            if(expenseId != null){
                for(debtor in e.debtors){
                    val newDebtor = Debtor(expenseId.toString(), debtor.username,
                        false, 0f)
                    debtorsRepository.removeDebtor(newDebtor)
                }
            }
            call.respond(HttpStatusCode.OK)
        }

        // Edit an existing expense
        post("/expenses/edit"){
            val request = call.receive<EditExpenseRequest>()

            expensesRepository.editExpense(request)
            call.respond(HttpStatusCode.OK)
        }

        // debtors functions -----------------------------------------------------------------------

        // Get all Debtors of an Expense
        get("/expense/debtors"){
            val id = call.parameters["id"]

            if(id == null){
                call.respond(HttpStatusCode.BadRequest)
            }
            else {
                val response = debtorsRepository.getDebtorByExpenseId(id.toInt())

                val userList = mutableListOf<UserResponse>()

                for (debtor in response) {
                    val user = usersRepository.getUserByUsername(debtor)
                    if (user != null) {
                        userList.add(user.toUserResponse())
                    }
                }

                call.respond(userList)
            }
        }

        // Get all Debtors
        get("/debtors"){
            val response = debtorsRepository.debtors()
            call.respond(response)
        }

        // Save a new Debtor
        post("/debtors/save") {
            val request = call.receive<Map<String, String>>()
            val values = request.values.toList()
            val debtorAdded = Debtor(values[0], values[1], values[2].toBoolean(), 50f)
            debtorsRepository.saveDebtor(debtorAdded)

            call.respondText (
                "Debtor created!",
                status = HttpStatusCode.Created)
        }

        // Add a PartialPayment
        post("/partialPayment/add"){
            val request = call.receive<ExpensePartialPaymentWrapper>()
            val expense = request.expense
            val value = request.partialPayment.value

            val expenseId = expensesRepository.getIdByExpenseRequest(expense.name,
                expense.description, expense.expenseDate, expense.totalValue)

            if(expenseId != null){
                expensesRepository.updateRemaining(expenseId, value)
                debtorsRepository.addPayment(expenseId, request.partialPayment.payer.username)
            }
            call.respond(HttpStatusCode.OK)
        }

        // Delete a PartialPayment
        post("/partialPayment/remove"){
            val request = call.receive<ExpensePartialPaymentWrapper>()
            val expense = request.expense

            val expenseId = expensesRepository.getIdByExpenseRequest(expense.name,
                expense.description, expense.expenseDate, expense.totalValue)

            if(expenseId != null){
                debtorsRepository.removePayment(expenseId, request.partialPayment.payer.username)
            }

            call.respond(HttpStatusCode.OK)
        }

        // Get all PartialPayments of an Expense
        get("/partialPayment/byExpense"){
            val name = call.parameters["name"]
            val description = call.parameters["description"]
            val expenseDate = call.parameters["expenseDate"]
            val totalValue = call.parameters["totalValue"]

            if(name == null || description == null || expenseDate == null || totalValue == null){
                call.respond(HttpStatusCode.BadRequest)
            }
            else {
                val expenseId = expensesRepository.getIdByExpenseRequest(
                    name,
                    description,
                    expenseDate,
                    totalValue
                )

                val response = mutableListOf<PartialPayment>()

                if (expenseId != null) {
                    val payersPairs = debtorsRepository.getUsernamesOfPartialPayers(expenseId)

                    for (payer in payersPairs) {
                        val userRequest = UserRequestWithoutPassword(
                            payer.first,
                            usersRepository.getNameByUsername(payer.first).toString()
                        )
                        val newPartialPayment = PartialPayment(userRequest, payer.second)
                        response.add(newPartialPayment)
                    }
                    call.respond(HttpStatusCode.OK, response)
                } else {
                    call.respond(HttpStatusCode.BadRequest)
                }
            }
        }
    }
}
