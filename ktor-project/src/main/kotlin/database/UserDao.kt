package database

import dto.UserRequest
import dto.UserRequestWithoutName
import dto.UserResponse
import kotlinx.coroutines.Dispatchers
import models.Budgets
import models.Budgets.id
import models.User
import models.Users
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import java.util.UUID

class UserDao {

    suspend fun findAllUsers(): List<User> = dbQuery {
        Users.selectAll().map {
            User(
                username = it[Users.username],
                name = it[Users.name],
                password = it[Users.password]
            )
        }
    }

    suspend fun getUserByUsername(username: String): User? {
        return transaction {
            Users.select { Users.username eq username }
                .map { row ->
                    User(
                        username = row[Users.username],
                        name = row[Users.name],
                        password = row[Users.password]
                    )
                }
                .singleOrNull()
        }
    }

    suspend fun saveUser(userRequest: UserRequest): User? = dbQuery {
        val insertStatement = Users.insert {
            it[username] = userRequest.username
            it[name] = userRequest.name
            it[password] = userRequest.password
        }

        insertStatement.resultedValues?.singleOrNull()?.let {
            User(
                username = it[Users.username],
                name = it[Users.name],
                password = it[Users.password]
            )
        }
    }



    suspend fun deleteUser(userName: String): Int {
        return transaction {
            val rowsAffected = Users.deleteWhere { Users.name eq userName }
            rowsAffected
        }
    }

    suspend fun authenticateUser(username: String, password: String): User? {
        return transaction {
            Users
                .select { (Users.username eq username) and (Users.password eq password) }
                .mapNotNull {
                    User(
                        username = it[Users.username],
                        password = it[Users.password],
                        name = it[Users.name]
                    )
                }
                .singleOrNull()
        }
    }

    suspend fun editName(username: String, newName: String) = dbQuery{
        Users.update ({ Users.username eq username}){
            it[name] = newName
        }
    }

    suspend fun getNameByUsername(username: String): String? {
        var name: String? = null

        transaction {
            val result = Users.select { Users.username eq username }.singleOrNull()
            name = result?.get(Users.name)
        }

        return name
    }
}