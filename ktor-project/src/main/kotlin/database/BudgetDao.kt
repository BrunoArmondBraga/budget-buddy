package database

import models.Budget
import models.Budgets
import models.Budgets.id
import models.Budgets.name
import models.Users
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.max
import org.jetbrains.exposed.sql.select

import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.update

class BudgetDao {

    suspend fun findAll(): List<Budget> = dbQuery {
        Budgets.selectAll().map {
            Budget(
                id = it[Budgets.id].toString(),
                name = it[Budgets.name]
            )
        }
    }

    suspend fun createBudget(newName: String, creator: String): Int? = dbQuery {
        val insertStatement = Budgets.insert {
            it[name] = newName
        }
        insertStatement.resultedValues?.singleOrNull()?.get(Budgets.id)
    }

    suspend fun findBudgetNameById(budgetId: Int): String? = dbQuery {
        Budgets
            .select { Budgets.id eq budgetId }
            .mapNotNull { it[Budgets.name] }
            .singleOrNull()
    }

    suspend fun getNextId(): Int {
        return newSuspendedTransaction {
            val maxId = Budgets.slice(Budgets.id.max()).selectAll().map { it[Budgets.id.max()] }.firstOrNull() ?: 0
            maxId + 1
        }
    }

    suspend fun changeBudgetName(budgetId: Int, newName: String) = dbQuery{
        Budgets.update ({ Budgets.id eq budgetId}){
            it[name] = newName
        }
    }

}