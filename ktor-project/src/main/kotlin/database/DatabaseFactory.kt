package database

//import models.Expenses
import kotlinx.coroutines.Dispatchers
import models.BudgetUserCorrelation
import models.Budgets
import models.Categories
import models.Debtors
import models.Expenses
import models.Users
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.transactions.transaction

object DatabaseFactory {
    fun init() {
        val driverClassName = "org.h2.Driver"
        val jdbcURL = "jdbc:h2:file:./build/db"
        val database = Database.connect(jdbcURL, driverClassName)

       transaction(database) {
           SchemaUtils.create(Expenses)
           SchemaUtils.create(Users)
           SchemaUtils.create(Budgets)
           SchemaUtils.create(BudgetUserCorrelation)
           SchemaUtils.create(Debtors)
           SchemaUtils.create(Categories)
       }
    }
}

suspend fun <T> dbQuery(block: suspend () -> T): T =
    newSuspendedTransaction(Dispatchers.IO) { block() }
