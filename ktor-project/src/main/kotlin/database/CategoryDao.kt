package database

import dto.CategoryRequest
import dto.CategoryResponse
import models.Budget
import models.BudgetUserCorrelation
import models.Budgets
import models.Categories
import models.Category
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update

class CategoryDao {

    suspend fun findAll(): List<CategoryResponse> = dbQuery{
        Categories.selectAll().map {
            CategoryResponse(
                budgetId = it[Categories.budgetId],
                name = it[Categories.name],
                icon = it[Categories.icon]
            )
        }
    }

    suspend fun createCategory(budgetId: Int, c: CategoryRequest) {
        dbQuery {
            Categories.insert {
                it[name] = c.name
                it[Categories.budgetId] = budgetId
                it[icon] = c.icon
            }
        }
    }

    suspend fun removeCategory(budgetId: Int, c: CategoryRequest) = dbQuery{
        Categories.deleteWhere {
            (Categories.budgetId eq budgetId) and (Categories.name eq c.name)
        }
    }

    suspend fun editCategory(budgetId: String, oldName: String, oldIcon: String, newName: String, newIcon: String) = dbQuery{
        Categories.update ({ (Categories.budgetId eq budgetId.toInt()) and (Categories.name eq oldName)}){
            it[name] = newName
            it[icon] = newIcon
        }
    }

    suspend fun getAllCategories(budgetId: Int): List<Category> {
        return transaction {
            Categories
                .select { Categories.budgetId eq budgetId }
                .map {
                    Category(
                        name = it[Categories.name],
                        icon = it[Categories.icon]
                    )
                }
        }
    }
}