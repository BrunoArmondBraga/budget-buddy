package database

import dto.UserResponse
import models.Budget
import models.BudgetUserCorrelation
import models.BudgetUserCorrelationTemplate
import models.Budgets
import models.User
import models.Users
import org.jetbrains.exposed.sql.JoinType
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll

class BudgetUserCorrelationDao {
    suspend fun findAll(): List<BudgetUserCorrelationTemplate> = dbQuery {
        BudgetUserCorrelation.selectAll().map {
            BudgetUserCorrelationTemplate(
                budgetId = it[BudgetUserCorrelation.budgetId],
                username = it[BudgetUserCorrelation.username]
            )
        }

    }

    suspend fun addUserToBudget(budgetId: Int, username: String) = dbQuery{
        BudgetUserCorrelation.insert{
            it[BudgetUserCorrelation.budgetId] = budgetId
            it[BudgetUserCorrelation.username] = username
        }
    }

    suspend fun removeUserFromBudget(budgetId: Int, username: String) = dbQuery {
        BudgetUserCorrelation.deleteWhere{
            (BudgetUserCorrelation.budgetId eq budgetId) and
                    (BudgetUserCorrelation.username eq username)
        }
    }

    suspend fun findUsersByBudget(budgetId: Int): List<UserResponse> = dbQuery {
        BudgetUserCorrelation
            .join(Users, JoinType.INNER, additionalConstraint = { BudgetUserCorrelation.username eq Users.username })
            .select { BudgetUserCorrelation.budgetId eq budgetId }
            .map {
                UserResponse(
                    username = it[Users.username],
                    name = it[Users.name]
                )
            }
    }

    suspend fun findBudgetsByUser(username: String): List<Budget> = dbQuery {
        BudgetUserCorrelation
            .join(Budgets, JoinType.INNER, additionalConstraint = { BudgetUserCorrelation.budgetId eq Budgets.id })
            .select { BudgetUserCorrelation.username eq username }
            .map {
                Budget(
                    id = it[Budgets.id].toString(),
                    name = it[Budgets.name]
                )
            }
    }
}