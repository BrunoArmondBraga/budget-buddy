package database

import dto.CategoryRequest
import dto.EditExpenseRequest
import dto.ExpenseRequest
import models.Categories
import models.Category
import models.Expense
import models.Expenses
import org.jetbrains.exposed.sql.SqlExpressionBuilder
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update

class ExpenseDao {

    suspend fun findAllExpenses(): List<Expense> = dbQuery {
        Expenses.selectAll().map {
            Expense(
                id = it[Expenses.id],
                budgetId = it[Expenses.budgetId],
                categoryName = it[Expenses.categoryName],
                name = it[Expenses.name],
                description = it[Expenses.description],
                expensePayer = it[Expenses.expensePayer],
                totalValue = it[Expenses.totalValue].toFloat(),
                remainingValue = it[Expenses.remainingValue].toFloat(),
                expenseDate = it[Expenses.expenseDate]
            )
        }
    }

    suspend fun addExpense(expenseRequest: Expense, numberOfDebtors: Int): Int = dbQuery {
        transaction {
            val insertStatement = Expenses.insert {
                it[budgetId] = expenseRequest.budgetId.toInt()
                it[categoryName] = expenseRequest.categoryName
                it[name] = expenseRequest.name
                it[description] = expenseRequest.description
                it[expensePayer] = expenseRequest.expensePayer
                it[totalValue] = expenseRequest.totalValue
                it[remainingValue] = expenseRequest.remainingValue
                it[expenseDate] = expenseRequest.expenseDate
            }

            val insertedId = insertStatement[Expenses.id].toInt()
            insertedId
        }
    }

    suspend fun deleteExpense(expenseRequest: Expense) = dbQuery {
        Expenses.deleteWhere {
            (Expenses.budgetId eq expenseRequest.budgetId) and
                    (Expenses.categoryName eq expenseRequest.categoryName) and
                    (Expenses.name eq expenseRequest.name) and
                    (Expenses.description eq expenseRequest.description) and
                    (Expenses.expenseDate eq expenseRequest.expenseDate) and
                    (Expenses.totalValue eq expenseRequest.totalValue)
        }
    }

    suspend fun editExpense(expense: EditExpenseRequest) {
        transaction {
            val existingExpense = Expenses.select { Expenses.name eq expense.expense.name }.singleOrNull()

            if (existingExpense != null) {
                Expenses.update({ Expenses.id eq existingExpense[Expenses.id] }) {
                    it[name] = expense.newName
                    it[description] = expense.newDescription
                    it[expenseDate] = expense.newDate
                }
            }
        }
    }


    suspend fun findExpensesByCategoryName(budgetId: Int, categoryName: String): List<Expense> = dbQuery {
        Expenses
            .select { (Expenses.budgetId eq budgetId) and (Expenses.categoryName eq categoryName) }
            .map {
                Expense(
                    id = it[Expenses.id],
                    budgetId = it[Expenses.budgetId],
                    categoryName = it[Expenses.categoryName],
                    name = it[Expenses.name],
                    description = it[Expenses.description],
                    expensePayer = it[Expenses.expensePayer],
                    totalValue = it[Expenses.totalValue],
                    remainingValue = it[Expenses.remainingValue],
                    expenseDate = it[Expenses.expenseDate]
                )
            }
    }

    suspend fun findExpensesByBudgetId(budgetId: Int): List<Expense> = dbQuery {
        Expenses
            .select { Expenses.budgetId eq budgetId }
            .map {
                Expense(
                    id = it[Expenses.id],
                    budgetId = it[Expenses.budgetId],
                    categoryName = it[Expenses.categoryName],
                    name = it[Expenses.name],
                    description = it[Expenses.description],
                    expensePayer = it[Expenses.expensePayer],
                    totalValue = it[Expenses.totalValue],
                    remainingValue = it[Expenses.remainingValue],
                    expenseDate = it[Expenses.expenseDate]
                )
            }
    }

    suspend fun getIdByExpense(expense: Expense): Int? = dbQuery {
        transaction {
            Expenses.select {
                (Expenses.budgetId eq expense.budgetId) and
                        (Expenses.categoryName eq expense.categoryName) and
                        (Expenses.name eq expense.name) and
                        (Expenses.description eq expense.description) and
                        (Expenses.expenseDate eq expense.expenseDate) and
                        (Expenses.totalValue eq expense.totalValue)
            }.singleOrNull()?.get(Expenses.id)?.toInt()
        }
    }

    suspend fun getIdByExpenseRequest(name: String,
                                      description: String,
                                      expenseDate: String,
                                      totalValue: String): Int? = dbQuery {
        transaction {
            Expenses.select {
                        (Expenses.name eq name) and
                        (Expenses.description eq description) and
                        (Expenses.expenseDate eq expenseDate) and
                        (Expenses.totalValue eq totalValue.toFloat())
            }.singleOrNull()?.get(Expenses.id)?.toInt()
        }
    }

    suspend fun editCategoryName(budgetId: Int, oldName: String, newName: String) {
        transaction {
            Expenses.update({ (Expenses.budgetId eq budgetId) and (Expenses.categoryName eq oldName) }) {
                it[categoryName] = newName
            }
        }
    }

    suspend fun updateRemaining(expenseId: Int, value: Float){
        transaction {
            Expenses.update ({ Expenses.id eq expenseId}){
                with(SqlExpressionBuilder){
                    it.update(Expenses.remainingValue, Expenses.remainingValue - value)
                }
            }
        }
    }
}