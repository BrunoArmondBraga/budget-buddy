package database

import models.Debtor
import models.Debtors
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update

class DebtorDao {

    suspend fun findAll(): List<Debtor> = dbQuery {
        Debtors.selectAll().map{
            Debtor(
                expenseId = it[Debtors.expenseId].toString(),
                username = it[Debtors.username].toString(),
                payed = it[Debtors.payed],
                value = it[Debtors.value]
            )
        }
    }

    suspend fun saveDebtor(debtor: Debtor) = dbQuery {
        val insertStatement = Debtors.insert {
            it[expenseId] = debtor.expenseId.toInt()
            it[username] = debtor.username
            it[payed] = debtor.payed
            it[value] = debtor.value
        }
    }

    suspend fun removeDebtor(debtor: Debtor) = dbQuery {
        Debtors.deleteWhere {
            (Debtors.expenseId eq debtor.expenseId.toInt()) and
                    (Debtors.username eq debtor.username)
        }
    }

    suspend fun getDebtorByExpenseId(expenseId: Int): List<String> {
        return transaction {
            Debtors
                .select { Debtors.expenseId eq expenseId }
                .map { it[Debtors.username] }
        }
    }

    suspend fun getUnpayedDebtorByExpenseId(expenseId: Int): List<String> {
        return transaction {
            Debtors
                .select { (Debtors.expenseId eq expenseId) and (Debtors.payed eq false) }
                .map { it[Debtors.username] }
        }
    }

    suspend fun addPayment(expenseId: Int, username: String){
        transaction {
            Debtors.update({ (Debtors.expenseId eq expenseId) and (Debtors.username eq username) }) {
                it[payed] = true
            }
        }
    }

    suspend fun removePayment(expenseId: Int, username: String){
        transaction {
            Debtors.update({ (Debtors.expenseId eq expenseId) and (Debtors.username eq username) }) {
                it[payed] = false
            }
        }
    }

    suspend fun getPayedPayments(expenseId: Int): List<Pair<String, Float>> {
        return transaction {
            Debtors
                .slice(Debtors.username, Debtors.value)
                .select { (Debtors.expenseId eq expenseId) and (Debtors.payed eq true) }
                .map { row ->
                    row[Debtors.username] to row[Debtors.value]
                }
        }
    }

}