package dto

import kotlinx.serialization.Serializable
import models.Expense
import models.User
import kotlin.math.exp

@Serializable
class ExpenseResponse(
    val id: String,
    val name: String,
    val description: String,
    val expensePayer: User,
    val expenseDate: String,
    val totalValue: Float,
    val remainingValue: Float,
    val debtors: List<User>
    )

fun Expense.toExpenseResponse(expensePayer: User, debtors: List<User>) = ExpenseResponse(
    id = id.toString(),
    name = name,
    description = description,
    totalValue = totalValue,
    remainingValue = remainingValue,
    expensePayer = expensePayer,
    expenseDate = expenseDate,
    debtors = debtors
)