package dto

import kotlinx.serialization.Serializable
import models.BudgetUserCorrelation
import models.BudgetUserCorrelationTemplate

@Serializable
class CorrelationResponse (
    val username: String,
    val budgetId: String
)

fun BudgetUserCorrelationTemplate.toBudgetCorrelationResponse() = CorrelationResponse(
    username = username,
    budgetId = budgetId.toString()
)