package dto

import kotlinx.serialization.Serializable
import models.User


@Serializable
class UserRequest (
    val username: String,
    val name: String,
    val password: String
) {
    fun toUser() = User(
        username = username,
        name = name,
        password = password
    )
}

@Serializable
class UserRequestWithoutName(
    val username: String,
    val password: String
)

@Serializable
class UserRequestWithoutPassword(
    val username: String,
    val name: String,
)