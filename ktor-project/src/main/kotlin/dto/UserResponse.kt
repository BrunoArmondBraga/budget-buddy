package dto

import kotlinx.serialization.Serializable
import models.User

@Serializable
class UserResponse(
    val username: String,
    val name: String
)

fun User.toUserResponse() = UserResponse(
    name = name,
    username = username
)