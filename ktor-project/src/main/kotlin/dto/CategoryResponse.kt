package dto

import kotlinx.serialization.Serializable

@Serializable
class CategoryResponse (
    val budgetId: Int,
    val name: String,
    val icon: String
)