package dto

import kotlinx.serialization.Serializable
import models.Budget
import models.Expense

@Serializable
class BudgetRequest (
    val name: String,
    val creator_username: String
)