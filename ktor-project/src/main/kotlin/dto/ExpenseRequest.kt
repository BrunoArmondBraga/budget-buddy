package dto

import kotlinx.serialization.Serializable
import models.Budget
import models.Category
import models.Expense
import models.User

@Serializable
class ExpenseRequest (
    val name: String,
    val description: String,
    val expensePayer: UserRequestWithoutPassword,
    val totalValue: String,
    val expenseDate: String,
    val debtors: List<UserRequestWithoutPassword>
) {
    fun toExpense(c: Category, b: Budget) = Expense(
        id = 0,
        budgetId = b.id.toInt(),
        categoryName = c.name,
        name = name,
        description = description,
        expensePayer = expensePayer.username,
        totalValue = totalValue.toFloat(),
        remainingValue = totalValue.toFloat() - totalValue.toFloat()/ (debtors.size + 1),
        expenseDate = expenseDate
    )
}

@Serializable
class EditExpenseRequest(
    val budget: Budget,
    val category: Category,
    val expense: ExpenseRequest,
    val newName: String,
    val newDescription: String,
    val newDate: String,
    val newReceipt: String
)