package dto

import kotlinx.serialization.Serializable
import models.Budget

@Serializable
class BudgetResponse (
    val id: String,
    val name: String
)

fun Budget.toBudgetResponse() = BudgetResponse(
    id = id.toString(),
    name = name
)