package dto

import kotlinx.serialization.Serializable

@Serializable
class CategoryRequest (
    val name: String,
    val icon: String
)

@Serializable
class EditCategoryRequest (
    val budgetId: String,
    val oldName: String,
    val oldIcon: String,
    val newName: String,
    val newIcon: String
)