package dto

import kotlinx.serialization.Serializable


@Serializable
class PartialPayment (
    val payer: UserRequestWithoutPassword,
    val value: Float,
    val receipt: String = ""
)